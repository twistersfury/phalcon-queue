<?php

namespace TwistersFury\Shared\Tests\Unit;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Manager;
use UnitTester;

class ManagerTest extends Unit
{
    /** @var Manager */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Manager();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
