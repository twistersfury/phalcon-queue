<?php

namespace TwistersFury\Shared\Tests\Unit\Job;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use UnitTester;

class JobInterfaceTest extends Unit
{
    /** @var JobInterface */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(JobInterface::class)
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
