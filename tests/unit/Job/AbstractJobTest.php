<?php

namespace TwistersFury\Shared\Tests\Unit\Job;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\AbstractJob;
use UnitTester;

class AbstractJobTest extends Unit
{
    /** @var AbstractModule */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractJob::class)
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
