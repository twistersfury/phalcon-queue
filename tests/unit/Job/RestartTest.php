<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Job;

use TwistersFury\Phalcon\Queue\Exceptions\Restart as RestartException;
use TwistersFury\Phalcon\Queue\Job\Restart as RestartJob;
use Codeception\Test\Unit;

class RestartTest extends Unit
{
    /** @var RestartJob */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new RestartJob();
    }

    public function testInstance()
    {
        $this->expectException(RestartException::class);
        $this->testSubject->handle();
    }
}
