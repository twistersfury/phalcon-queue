<?php

namespace TwistersFury\Shared\Tests\Unit\Job;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\KeepAlive;
use UnitTester;

class KeepAliveTest extends Unit
{
    /** @var KeepAlive */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new KeepAlive();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
