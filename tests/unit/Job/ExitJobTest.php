<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Job;

use TwistersFury\Phalcon\Queue\Exceptions\ExitException;
use TwistersFury\Phalcon\Queue\Job\ExitJob;
use Codeception\Test\Unit;

class ExitJobTest extends Unit
{
    /** @var ExitJob */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new ExitJob();
    }

    public function testInstance()
    {
        $this->expectException(ExitException::class);
        $this->testSubject->handle();
    }
}
