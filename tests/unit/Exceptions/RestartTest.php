<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Exceptions;

use RuntimeException;
use TwistersFury\Phalcon\Queue\Exceptions\Restart;
use Codeception\Test\Unit;

class RestartTest extends Unit
{
    /** @var Restart */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Restart();
    }

    public function testInstance()
    {
        $this->assertInstanceOf(RuntimeException::class, $this->testSubject);
    }
}
