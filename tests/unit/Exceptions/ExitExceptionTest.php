<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Exceptions;

use RuntimeException;
use TwistersFury\Phalcon\Queue\Exceptions\ExitException;
use Codeception\Test\Unit;

class ExitExceptionTest extends Unit
{
    /** @var ExitException */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new ExitException();
    }

    public function testInstance()
    {
        $this->assertInstanceOf(RuntimeException::class, $this->testSubject);
    }
}
