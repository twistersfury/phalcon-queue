<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Exceptions;

use RuntimeException;
use TwistersFury\Phalcon\Queue\Exceptions\FatalException;
use Codeception\Test\Unit;

class FatalExceptionTest extends Unit
{
    /** @var FatalException */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new FatalException();
    }

    public function testInstance()
    {
        $this->assertInstanceOf(RuntimeException::class, $this->testSubject);
    }
}
