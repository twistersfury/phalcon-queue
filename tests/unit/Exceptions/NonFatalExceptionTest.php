<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Exceptions;

use RuntimeException;
use TwistersFury\Phalcon\Queue\Exceptions\NonFatalException;
use Codeception\Test\Unit;

class NonFatalExceptionTest extends Unit
{
    /** @var NonFatalException */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new NonFatalException();
    }

    public function testInstance()
    {
        $this->assertInstanceOf(RuntimeException::class, $this->testSubject);
    }
}
