<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Cli\Task;

use Codeception\Stub;
use Phalcon\Cli\Dispatcher;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;
use TwistersFury\Phalcon\Queue\Cli\Task\RunJobTask;
use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class RunJobTaskTest extends Unit
{
    /** @var RunJobTask */
    private $testSubject;

    /** @var Di */
    private $di;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new RunJobTask();
        $this->di = new Di();
        $this->testSubject->setDI($this->di);

        $this->di->set("logger", Stub::makeEmpty(
            Logger::class,
            [
                'info' => Stub\Expected::atLeastOnce()
            ]
        ));
    }

    public function testMainAction()
    {
        $mockJob = Stub::makeEmpty(JobInterface::class);
        $mockDispatcher = Stub::makeEmpty(
            Dispatcher::class,
            [
                "getOption" => "some-job"
            ]
        );

        $this->di->set("some-job", $mockJob);
        $this->di->set("dispatcher", $mockDispatcher);

        $this->di->set(
            "queue",
            Stub::makeEmpty(
                Beanstalk::class,
                [
                    'put' => Stub\Expected::atLeastOnce($mockJob)
                ]
            )
        );

        $this->assertSame($this->testSubject, $this->testSubject->mainAction("some-job"));
    }
}
