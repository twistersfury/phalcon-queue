<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Cli\Task;

use Codeception\Stub;
use DateTime;
use Phalcon\Cache\Cache;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;
use TwistersFury\Phalcon\Queue\Cli\Task\QueueTask;
use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\Phalcon\Queue\Job\KeepAlive;
use TwistersFury\Phalcon\Queue\Exceptions\Restart;

class QueueTaskTest extends Unit
{
    /** @var QueueTask */
    private $testSubject;

    /** @var Di */
    private $di;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->di = new Di();

        $this->di->set(
            'DateTime',
            function () {
                return new DateTime('2022-01-01 20:10:10');
            }
        );

        $this->di->set("logger", Stub::makeEmpty(
            Logger::class,
            [
                'info' => Stub\Expected::atLeastOnce()
            ]
        ));

        $this->di->set("cache", Stub::makeEmpty(
            Cache::class,
            [
                'has' => Stub\Expected::atLeastOnce(false),
            ]
        ));

        $this->testSubject = new QueueTask();
        $this->testSubject->setDI($this->di);
        $this->testSubject->initialize();
    }

    public function testConsumeAction()
    {
        $this->di->set(
            "queue",
            Stub::makeEmpty(
                Beanstalk::class,
                [
                    'reserve' => Stub::consecutive(
                        Stub::makeEmpty(
                        Beanstalk\Job::class,
                            [
                                'getBody' => Stub::makeEmpty(
                                    JobInterface::class,
                                    [
                                        'handle' => Stub\Expected::atLeastOnce()
                                    ]
                                ),
                                'delete' => Stub\Expected::atLeastOnce(function () {
                                    return true;
                                })
                            ]
                        ),
                        false
                    )
                ]
            )
        );

        $this->assertSame($this->testSubject, $this->testSubject->consumeAction());
    }

    public function testKeepAlive()
    {
        $mockKeep = Stub::makeEmpty(KeepAlive::class);

        $mockQueue = Stub::makeEmpty(
            Beanstalk::class,
            [
                'reserve' => Stub::consecutive(
                    Stub::makeEmpty(
                        Beanstalk\Job::class,
                        [
                            'getBody' => $mockKeep
                        ]
                    ),
                    false
                ),
                'put' => Stub\Expected::once($mockKeep)
            ]
        );

        $this->di->set(
            "queue",
            $mockQueue
        );

        $this->di->set(
            KeepAlive::class,
            $mockKeep
        );

        $this->assertSame($this->testSubject, $this->testSubject->consumeAction());
    }

    public function testRestart()
    {
        $this->di->set(
            "queue",
            $this->makeEmpty(
                Beanstalk::class,
                [
                    'reserve' => Stub\Expected::once(
                        $this->makeEmpty(
                            Beanstalk\Job::class,
                            [
                                'getBody' => Stub\Expected::exactly(
                                    3,
                                    $this->makeEmpty(
                                        JobInterface::class,
                                        [
                                            'handle' => Stub\Expected::once(
                                                function () {
                                                    throw new Restart();
                                                }
                                            )
                                        ]
                                    )
                                ),
                            ]
                        )
                    )
                ]
            )
        );

        $returnDate = '2022-01-01 10:10:10';

        $this->di->set("cache", $this->makeEmpty(
            Cache::class,
            [
                'has' => Stub\Expected::exactly(2, true),
                'set' => Stub\Expected::once(
                    function ($key, $value) use (&$returnDate) {
                        $this->assertEquals('TwistersFury.Phalcon.Queue.Task.Restart', $key);
                        $this->assertInstanceOf(\DateTime::class, $value);
                        $this->assertEquals('2022-01-01 20:10:10', $value->format('Y-m-d H:i:s'));

                        $returnDate = $value->format('Y-m-d H:i:s');

                        return true;
                    }
                ),
                'get' => function () use (&$returnDate) {
                    return new DateTime($returnDate);
                }
            ]
        ));

        $this->di->set("logger", $this->makeEmpty(
            Logger::class,
            [
                'info' => Stub\Expected::atLeastOnce(),
                'notice' => Stub\Expected::atLeastOnce()
            ]
        ));

        $this->assertSame($this->testSubject, $this->testSubject->consumeAction());
    }
}
