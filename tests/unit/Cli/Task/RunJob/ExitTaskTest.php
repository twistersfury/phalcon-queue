<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit\Cli\Task;

use Codeception\Stub;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;
use TwistersFury\Phalcon\Queue\Cli\Task\RunJob\ExitTask;
use Codeception\Test\Unit;
use TwistersFury\Phalcon\Queue\Job\ExitJob;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class ExitTaskTest extends Unit
{
    /** @var ExitTask */
    private $testSubject;

    /** @var Di */
    private $di;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new ExitTask();
        $this->di = new Di();
        $this->testSubject->setDI($this->di);

        $this->di->set("logger", Stub::makeEmpty(
            Logger::class,
            [
                'info' => Stub\Expected::atLeastOnce()
            ]
        ));
    }

    public function testMainAction()
    {
        $mockJob = Stub::makeEmpty(JobInterface::class);

        $this->di->set(ExitJob::class, $mockJob);

        $this->di->set(
            "queue",
            Stub::makeEmpty(
                Beanstalk::class,
                [
                    'put' => Stub\Expected::atLeastOnce($mockJob)
                ]
            )
        );

        $this->assertSame($this->testSubject, $this->testSubject->mainAction());
    }
}
