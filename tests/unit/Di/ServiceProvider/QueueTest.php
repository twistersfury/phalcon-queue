<?php

namespace TwistersFury\Phalcon\Queue\Tests\Unit;

use Codeception\Stub;
use Phalcon\Di\Di;
use Phalcon\Config\Config;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;
use TwistersFury\Phalcon\Queue\Di\ServiceProvider\Queue;
use Codeception\Test\Unit;

class QueueTest extends Unit
{
    /** @var Queue */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Queue();
    }

    public function testDi()
    {
        $mockDi = new Di();
        $mockDi->set("config", new Config());
        $this->testSubject->register($mockDi);

        $this->assertTrue($mockDi->has("queue"));
    }

    public function testQueues()
    {
        $mockDi = new Di();
        $mockDi->set(
            "config",
            new Config([
                "queues" => [
                    "testQueue" => [
                        "host" => 'test.host',
                        "port" => 11300,
                        "tube" => "testTube"
                    ]
                ]
            ])
        );

        $mockDi->set("TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk", Stub::makeEmpty(Beanstalk::class));

        $this->testSubject->register($mockDi);

        $this->assertTrue($mockDi->has("testQueue"));
    }
}
