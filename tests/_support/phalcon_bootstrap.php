<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

if (!defined('TEST_BT_HOST')) {
    define('TEST_BT_HOST', 'beanstalk.twistersfury.test');
    define('TEST_BT_PORT', 11300);
}

return new Application(new FactoryDefault());
