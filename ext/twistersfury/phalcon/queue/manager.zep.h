
extern zend_class_entry *twistersfury_phalcon_queue_manager_ce;

ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Manager);

PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, getDriver);
PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, put);
PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, reserve);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_manager_getdriver, 0, 1, TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk, 0)
	ZEND_ARG_TYPE_INFO(0, queueName, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_manager_put, 0, 2, TwistersFury\\Phalcon\\Queue\\Manager, 0)
	ZEND_ARG_TYPE_INFO(0, queueName, IS_STRING, 0)
	ZEND_ARG_OBJ_INFO(0, job, TwistersFury\\Phalcon\\Queue\\Job\\JobInterface, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, options, IS_ARRAY, 1, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, options, 1)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_manager_reserve, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, queueName, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, timeout, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_manager_method_entry) {
	PHP_ME(TwistersFury_Phalcon_Queue_Manager, getDriver, arginfo_twistersfury_phalcon_queue_manager_getdriver, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Manager, put, arginfo_twistersfury_phalcon_queue_manager_put, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Manager, reserve, arginfo_twistersfury_phalcon_queue_manager_reserve, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
