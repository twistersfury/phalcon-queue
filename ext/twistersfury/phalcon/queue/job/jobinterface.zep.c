
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Job_JobInterface)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\Phalcon\\Queue\\Job, JobInterface, twistersfury_phalcon_queue, job_jobinterface, twistersfury_phalcon_queue_job_jobinterface_method_entry);

	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_Phalcon_Queue_Job_JobInterface, handle);
ZEPHIR_DOC_METHOD(TwistersFury_Phalcon_Queue_Job_JobInterface, getOptions);
