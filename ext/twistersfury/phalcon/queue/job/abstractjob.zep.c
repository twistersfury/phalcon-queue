
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Job_AbstractJob)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Job, AbstractJob, twistersfury_phalcon_queue, job_abstractjob, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\abstractconfiginjectable")), twistersfury_phalcon_queue_job_abstractjob_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_class_implements(twistersfury_phalcon_queue_job_abstractjob_ce, 1, twistersfury_phalcon_queue_job_jobinterface_ce);
	zend_class_implements(twistersfury_phalcon_queue_job_abstractjob_ce, 1, zephir_get_internal_ce(SL("phalcon\\di\\injectionawareinterface")));
	zend_class_implements(twistersfury_phalcon_queue_job_abstractjob_ce, 1, zend_ce_serializable);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Job_AbstractJob, getOptions)
{
	zval *this_ptr = getThis();



	array_init(return_value);
	return;
}

