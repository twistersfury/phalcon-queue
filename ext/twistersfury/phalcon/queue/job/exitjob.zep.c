
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/exception.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Job_ExitJob)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Job, ExitJob, twistersfury_phalcon_queue, job_exitjob, twistersfury_phalcon_queue_job_abstractjob_ce, twistersfury_phalcon_queue_job_exitjob_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Job_ExitJob, handle)
{
	zval *this_ptr = getThis();



	ZEPHIR_THROW_EXCEPTION_DEBUG_STRW(twistersfury_phalcon_queue_exceptions_exitexception_ce, "Exit Job", "twistersfury/phalcon/queue/Job/ExitJob.zep", 11);
	return;
}

