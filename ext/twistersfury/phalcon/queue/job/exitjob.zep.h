
extern zend_class_entry *twistersfury_phalcon_queue_job_exitjob_ce;

ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Job_ExitJob);

PHP_METHOD(TwistersFury_Phalcon_Queue_Job_ExitJob, handle);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_job_exitjob_handle, 0, 0, TwistersFury\\Phalcon\\Queue\\Job\\JobInterface, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_job_exitjob_method_entry) {
	PHP_ME(TwistersFury_Phalcon_Queue_Job_ExitJob, handle, arginfo_twistersfury_phalcon_queue_job_exitjob_handle, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
