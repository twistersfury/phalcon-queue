
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Di\\ServiceProvider, Queue, twistersfury_phalcon_queue, di_serviceprovider_queue, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\serviceprovider\\abstractserviceprovider")), twistersfury_phalcon_queue_di_serviceprovider_queue_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue, registerQueues)
{
	zval _11$$5, _14$$6;
	zend_string *_10;
	zend_ulong _9;
	zval _0, _1, _2, _3, instance, config, name, _4, _5, _6, *_7, _8, _12$$5, _15$$6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_13 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&instance);
	ZVAL_UNDEF(&config);
	ZVAL_UNDEF(&name);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_12$$5);
	ZVAL_UNDEF(&_15$$6);
	ZVAL_UNDEF(&_11$$5);
	ZVAL_UNDEF(&_14$$6);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "config");
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "get", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "queues");
	ZEPHIR_CALL_METHOD(&_2, &_0, "has", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "baseQueue");
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "has", NULL, 0, &_1);
	zephir_check_call_status();
	if (!(zephir_is_true(&_2))) {
		RETURN_MM_NULL();
	} else if (!(zephir_is_true(&_3))) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "registerbasequeue", NULL, 0);
		zephir_check_call_status();
	}
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "config");
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "get", NULL, 0, &_1);
	zephir_check_call_status();
	zephir_read_property(&_5, &_4, ZEND_STRL("queues"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_6, &_5, "getiterator", NULL, 0);
	zephir_check_call_status();
	zephir_is_iterable(&_6, 0, "twistersfury/phalcon/queue/Di/ServiceProvider/Queue.zep", 26);
	if (Z_TYPE_P(&_6) == IS_ARRAY) {
		ZEND_HASH_FOREACH_KEY_VAL(Z_ARRVAL_P(&_6), _9, _10, _7)
		{
			ZEPHIR_INIT_NVAR(&name);
			if (_10 != NULL) { 
				ZVAL_STR_COPY(&name, _10);
			} else {
				ZVAL_LONG(&name, _9);
			}
			ZEPHIR_INIT_NVAR(&config);
			ZVAL_COPY(&config, _7);
			ZEPHIR_INIT_NVAR(&_11$$5);
			zephir_create_array(&_11$$5, 1, 0);
			zephir_array_fast_append(&_11$$5, &config);
			ZEPHIR_INIT_NVAR(&_12$$5);
			ZVAL_STRING(&_12$$5, "baseQueue");
			ZEPHIR_CALL_METHOD(&instance, this_ptr, "get", NULL, 0, &_12$$5, &_11$$5);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "setshared", &_13, 0, &name, &instance);
			zephir_check_call_status();
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &_6, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_8, &_6, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_8)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&name, &_6, "key", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&config, &_6, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_14$$6);
				zephir_create_array(&_14$$6, 1, 0);
				zephir_array_fast_append(&_14$$6, &config);
				ZEPHIR_INIT_NVAR(&_15$$6);
				ZVAL_STRING(&_15$$6, "baseQueue");
				ZEPHIR_CALL_METHOD(&instance, this_ptr, "get", NULL, 0, &_15$$6, &_14$$6);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(NULL, this_ptr, "setshared", &_13, 0, &name, &instance);
				zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, &_6, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&config);
	ZEPHIR_INIT_NVAR(&name);
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue, registerManager)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "queueManager");
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "TwistersFury\\Phalcon\\Queue\\Manager");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setshared", NULL, 0, &_0, &_1);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue, registerQueue)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_INIT_NVAR(&_0);
	zephir_create_closure_ex(&_0, NULL, twistersfury_phalcon_queue_0__closure_ce, SL("__invoke"));
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "queue");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setshared", NULL, 0, &_1, &_0);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue, registerBaseQueue)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_INIT_NVAR(&_0);
	zephir_create_closure_ex(&_0, NULL, twistersfury_phalcon_queue_1__closure_ce, SL("__invoke"));
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "baseQueue");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "set", NULL, 0, &_1, &_0);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

