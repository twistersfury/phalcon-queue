
extern zend_class_entry *twistersfury_phalcon_queue_1__closure_ce;

ZEPHIR_INIT_CLASS(twistersfury_phalcon_queue_1__closure);

PHP_METHOD(twistersfury_phalcon_queue_1__closure, __invoke);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_1__closure___invoke, 0, 0, 0)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 1)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_1__closure_method_entry) {
	PHP_ME(twistersfury_phalcon_queue_1__closure, __invoke, arginfo_twistersfury_phalcon_queue_1__closure___invoke, ZEND_ACC_PUBLIC|ZEND_ACC_FINAL)
	PHP_FE_END
};
