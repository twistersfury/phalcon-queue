
extern zend_class_entry *twistersfury_phalcon_queue_cli_task_queuetask_ce;

ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask);

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getStartTime);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, consumeAction);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, processJob);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, canProcess);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, buildDate);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getRestartKey);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getNow);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, registerRestart);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getstarttime, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_consumeaction, 0, 0, TwistersFury\\Phalcon\\Queue\\Cli\\Task\\QueueTask, 0)
	ZEND_ARG_TYPE_INFO(0, serviceName, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, maxItems, IS_LONG, 0)
	ZEND_ARG_TYPE_INFO(0, reserveTimeout, IS_LONG, 0)
	ZEND_ARG_TYPE_INFO(0, sleepMs, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_processjob, 0, 1, TwistersFury\\Phalcon\\Queue\\Cli\\Task\\QueueTask, 0)
	ZEND_ARG_OBJ_INFO(0, job, TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk\\Job, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_canprocess, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_builddate, 0, 1, DateTime, 0)
	ZEND_ARG_TYPE_INFO(0, dateTime, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getrestartkey, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getnow, 0, 0, DateTime, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_queuetask_registerrestart, 0, 0, TwistersFury\\Phalcon\\Queue\\Cli\\Task\\QueueTask, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_cli_task_queuetask_method_entry) {
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getStartTime, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getstarttime, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getStartTime, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, consumeAction, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_consumeaction, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, processJob, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_processjob, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, canProcess, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_canprocess, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, buildDate, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_builddate, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getRestartKey, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getrestartkey, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getNow, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_getnow, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, registerRestart, arginfo_twistersfury_phalcon_queue_cli_task_queuetask_registerrestart, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
