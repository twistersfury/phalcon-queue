
#ifdef HAVE_CONFIG_H
#include "../../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../../php_ext.h"
#include "../../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Cli_Task_RunJob_ExitTask)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Cli\\Task\\RunJob, ExitTask, twistersfury_phalcon_queue, cli_task_runjob_exittask, twistersfury_phalcon_queue_cli_task_runjobtask_ce, twistersfury_phalcon_queue_cli_task_runjob_exittask_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJob_ExitTask, getJobClass)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury\\Phalcon\\Queue\\Job\\ExitJob");
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJob_ExitTask, getOptions)
{
	zval _2;
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_1 = NULL;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(&_0, twistersfury_phalcon_queue_cli_task_runjob_exittask_ce, getThis(), "getoptions", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	zephir_create_array(&_2, 1, 0);
	add_assoc_long_ex(&_2, SL("priority"), 0);
	zephir_fast_array_merge(return_value, &_0, &_2);
	RETURN_MM();
}

