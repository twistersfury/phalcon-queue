
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/operators.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/exception.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/memory.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Cli\\Task, RunJobTask, twistersfury_phalcon_queue, cli_task_runjobtask, zephir_get_internal_ce(SL("phalcon\\cli\\task")), twistersfury_phalcon_queue_cli_task_runjobtask_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, mainAction)
{
	zval _2;
	zend_bool _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *className_param = NULL, *serviceName_param = NULL, _1, _3, _4, _5, _6, _7, _8, jobClass$$3;
	zval className, serviceName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&className);
	ZVAL_UNDEF(&serviceName);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&jobClass$$3);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(className)
		Z_PARAM_STR(serviceName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 2, &className_param, &serviceName_param);
	if (!className_param) {
		ZEPHIR_INIT_VAR(&className);
		ZVAL_STRING(&className, "");
	} else {
	if (UNEXPECTED(Z_TYPE_P(className_param) != IS_STRING && Z_TYPE_P(className_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'className' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(className_param) == IS_STRING)) {
		zephir_get_strval(&className, className_param);
	} else {
		ZEPHIR_INIT_VAR(&className);
	}
	}
	if (!serviceName_param) {
		ZEPHIR_INIT_VAR(&serviceName);
		ZVAL_STRING(&serviceName, "queue");
	} else {
	if (UNEXPECTED(Z_TYPE_P(serviceName_param) != IS_STRING && Z_TYPE_P(serviceName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'serviceName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(serviceName_param) == IS_STRING)) {
		zephir_get_strval(&serviceName, serviceName_param);
	} else {
		ZEPHIR_INIT_VAR(&serviceName);
	}
	}


	_0 = ZEPHIR_IS_STRING_IDENTICAL(&className, "");
	if (_0) {
		_0 = (zephir_method_exists_ex(this_ptr, ZEND_STRL("getjobclass")) == SUCCESS);
	}
	if (_0) {
		ZEPHIR_CALL_METHOD(&jobClass$$3, this_ptr, "getjobclass", NULL, 0);
		zephir_check_call_status();
		zephir_get_strval(&className, &jobClass$$3);
	}
	if (!(!(ZEPHIR_IS_EMPTY(&className)))) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(spl_ce_InvalidArgumentException, "Invalid Class Name", "twistersfury/phalcon/queue/Cli/Task/RunJobTask.zep", 16);
		return;
	}
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_2);
	zephir_create_array(&_2, 1, 0);
	zephir_array_update_string(&_2, SL("job"), &className, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "Running Job");
	ZEPHIR_CALL_METHOD(NULL, &_1, "info", NULL, 0, &_3, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, &_4, "get", NULL, 0, &serviceName);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &className);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_8, this_ptr, "getoptions", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_5, "put", NULL, 0, &_7, &_8);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, getOptions)
{
	zval *this_ptr = getThis();



	array_init(return_value);
	return;
}

