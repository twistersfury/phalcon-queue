
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "ext/spl/spl_exceptions.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Cli\\Task, QueueTask, twistersfury_phalcon_queue, cli_task_queuetask, zephir_get_internal_ce(SL("phalcon\\cli\\task")), twistersfury_phalcon_queue_cli_task_queuetask_method_entry, 0);

	zend_declare_property_null(twistersfury_phalcon_queue_cli_task_queuetask_ce, SL("startTime"), ZEND_ACC_PRIVATE);
	zephir_declare_class_constant_string(twistersfury_phalcon_queue_cli_task_queuetask_ce, SL("RESTART_KEY"), "TwistersFury.Phalcon.Queue.Task.Restart");

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getStartTime)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "startTime");
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, consumeAction)
{
	zend_bool requeue, _6, _7;
	zval _1, _15$$3, _19$$8;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_5 = NULL, *_9 = NULL, *_13 = NULL, *_17 = NULL;
	zend_long maxItems, reserveTimeout, sleepMs, ZEPHIR_LAST_CALL_STATUS, count;
	zval *serviceName_param = NULL, *maxItems_param = NULL, *reserveTimeout_param = NULL, *sleepMs_param = NULL, _0, _2, job, exception, _3, _4, _28, _8$$3, _10$$3, _11$$3, _14$$3, _16$$3, _12$$4, _18$$8, _20$$8, _21$$8, _22$$9, _23$$10, _24$$10, _25$$10, _26$$10, _27$$10;
	zval serviceName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&serviceName);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&job);
	ZVAL_UNDEF(&exception);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_28);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_14$$3);
	ZVAL_UNDEF(&_16$$3);
	ZVAL_UNDEF(&_12$$4);
	ZVAL_UNDEF(&_18$$8);
	ZVAL_UNDEF(&_20$$8);
	ZVAL_UNDEF(&_21$$8);
	ZVAL_UNDEF(&_22$$9);
	ZVAL_UNDEF(&_23$$10);
	ZVAL_UNDEF(&_24$$10);
	ZVAL_UNDEF(&_25$$10);
	ZVAL_UNDEF(&_26$$10);
	ZVAL_UNDEF(&_27$$10);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_15$$3);
	ZVAL_UNDEF(&_19$$8);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 4)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR(serviceName)
		Z_PARAM_LONG(maxItems)
		Z_PARAM_LONG(reserveTimeout)
		Z_PARAM_LONG(sleepMs)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 4, &serviceName_param, &maxItems_param, &reserveTimeout_param, &sleepMs_param);
	if (!serviceName_param) {
		ZEPHIR_INIT_VAR(&serviceName);
		ZVAL_STRING(&serviceName, "queue");
	} else {
	if (UNEXPECTED(Z_TYPE_P(serviceName_param) != IS_STRING && Z_TYPE_P(serviceName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'serviceName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(serviceName_param) == IS_STRING)) {
		zephir_get_strval(&serviceName, serviceName_param);
	} else {
		ZEPHIR_INIT_VAR(&serviceName);
	}
	}
	if (!maxItems_param) {
		maxItems = 0;
	} else {
		maxItems = zephir_get_intval(maxItems_param);
	}
	if (!reserveTimeout_param) {
		reserveTimeout = 0;
	} else {
		reserveTimeout = zephir_get_intval(reserveTimeout_param);
	}
	if (!sleepMs_param) {
		sleepMs = 0;
	} else {
		sleepMs = zephir_get_intval(sleepMs_param);
	}


	count = 0;
	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 2, 0);
	zephir_array_update_string(&_1, SL("service"), &serviceName, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_LONG(&_2, maxItems);
	zephir_array_update_string(&_1, SL("maxItems"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Queue Started");
	ZEPHIR_CALL_METHOD(NULL, &_0, "info", NULL, 0, &_2, &_1);
	zephir_check_call_status();
	requeue = 0;
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getnow", NULL, 0);
	zephir_check_call_status();
	zephir_update_property_zval(this_ptr, ZEND_STRL("startTime"), &_3);
	while (1) {
		ZEPHIR_CALL_METHOD(&_4, this_ptr, "canprocess", &_5, 0);
		zephir_check_call_status();
		_6 = zephir_is_true(&_4);
		if (_6) {
			_7 = maxItems == 0;
			if (!(_7)) {
				_7 = count < maxItems;
			}
			_6 = _7;
		}
		if (!(_6)) {
			break;
		}
		ZEPHIR_CALL_METHOD(&_8$$3, this_ptr, "getdi", &_9, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_10$$3, &_8$$3, "get", NULL, 0, &serviceName);
		zephir_check_call_status();
		ZVAL_LONG(&_11$$3, reserveTimeout);
		ZEPHIR_CALL_METHOD(&job, &_10$$3, "reserve", NULL, 0, &_11$$3);
		zephir_check_call_status();
		if (ZEPHIR_IS_FALSE_IDENTICAL(&job)) {
			if (sleepMs == 0) {
				break;
			}
			ZVAL_LONG(&_12$$4, sleepMs);
			ZEPHIR_CALL_FUNCTION(NULL, "usleep", &_13, 13, &_12$$4);
			zephir_check_call_status();
			continue;
		} else if (zephir_instance_of_ev(&job, twistersfury_phalcon_queue_job_keepalive_ce)) {
			ZEPHIR_CALL_METHOD(NULL, &job, "delete", NULL, 0);
			zephir_check_call_status();
			requeue = 1;
			continue;
		}
		count = (count + 1);
		ZEPHIR_OBS_NVAR(&_14$$3);
		zephir_read_property(&_14$$3, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
		ZEPHIR_INIT_NVAR(&_15$$3);
		zephir_create_array(&_15$$3, 1, 0);
		ZEPHIR_INIT_NVAR(&_16$$3);
		ZVAL_LONG(&_16$$3, count);
		zephir_array_update_string(&_15$$3, SL("count"), &_16$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_NVAR(&_16$$3);
		ZVAL_STRING(&_16$$3, "Queue Loop Count");
		ZEPHIR_CALL_METHOD(NULL, &_14$$3, "debug", NULL, 0, &_16$$3, &_15$$3);
		zephir_check_call_status();

		/* try_start_1: */

			ZEPHIR_CALL_METHOD(NULL, this_ptr, "processjob", &_17, 0, &job);
			zephir_check_call_status_or_jump(try_end_1);

		try_end_1:

		if (EG(exception)) {
			ZEPHIR_INIT_NVAR(&_16$$3);
			ZVAL_OBJ(&_16$$3, EG(exception));
			Z_ADDREF_P(&_16$$3);
			if (zephir_instance_of_ev(&_16$$3, twistersfury_phalcon_queue_exceptions_exitexception_ce)) {
				zend_clear_exception();
				ZEPHIR_CPY_WRT(&exception, &_16$$3);
				ZEPHIR_OBS_NVAR(&_18$$8);
				zephir_read_property(&_18$$8, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
				ZEPHIR_INIT_NVAR(&_19$$8);
				zephir_create_array(&_19$$8, 1, 0);
				ZEPHIR_CALL_METHOD(&_20$$8, &exception, "getmessage", NULL, 0);
				zephir_check_call_status();
				zephir_array_update_string(&_19$$8, SL("message"), &_20$$8, PH_COPY | PH_SEPARATE);
				ZEPHIR_INIT_NVAR(&_21$$8);
				ZVAL_STRING(&_21$$8, "Exit Initialized");
				ZEPHIR_CALL_METHOD(NULL, &_18$$8, "notice", NULL, 0, &_21$$8, &_19$$8);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(NULL, &job, "delete", NULL, 0);
				zephir_check_call_status();
				zephir_throw_exception_debug(&exception, "twistersfury/phalcon/queue/Cli/Task/QueueTask.zep", 82);
				ZEPHIR_MM_RESTORE();
				return;
			}
		}
		if (sleepMs > 0) {
			ZVAL_LONG(&_22$$9, sleepMs);
			ZEPHIR_CALL_FUNCTION(NULL, "usleep", &_13, 13, &_22$$9);
			zephir_check_call_status();
		}
	}
	if (requeue == 1) {
		ZEPHIR_CALL_METHOD(&_23$$10, this_ptr, "getdi", &_9, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_24$$10, &_23$$10, "get", NULL, 0, &serviceName);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_25$$10, this_ptr, "getdi", &_9, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_27$$10);
		ZVAL_STRING(&_27$$10, "TwistersFury\\Phalcon\\Queue\\Job\\KeepAlive");
		ZEPHIR_CALL_METHOD(&_26$$10, &_25$$10, "get", NULL, 0, &_27$$10);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, &_24$$10, "put", NULL, 0, &_26$$10);
		zephir_check_call_status();
	}
	ZEPHIR_OBS_VAR(&_28);
	zephir_read_property(&_28, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Queue Ended");
	ZEPHIR_CALL_METHOD(NULL, &_28, "info", NULL, 0, &_2);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, processJob)
{
	zval _11$$3, _4$$4, _17$$5, _21$$9, _25$$11, _30$$13;
	zend_bool _1$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *job, job_sub, exception, _15, _35, _36, _0$$3, _2$$3, _10$$3, _12$$3, _13$$3, _14$$3, _3$$4, _5$$4, _6$$4, _7$$4, _8$$4, _9$$4, _16$$5, _18$$5, _19$$5, _20$$9, _22$$9, _23$$9, _24$$11, _26$$11, _27$$11, _28$$11, _29$$13, _31$$13, _32$$13, _33$$15, _34$$15;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&job_sub);
	ZVAL_UNDEF(&exception);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_35);
	ZVAL_UNDEF(&_36);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_13$$3);
	ZVAL_UNDEF(&_14$$3);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_5$$4);
	ZVAL_UNDEF(&_6$$4);
	ZVAL_UNDEF(&_7$$4);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);
	ZVAL_UNDEF(&_16$$5);
	ZVAL_UNDEF(&_18$$5);
	ZVAL_UNDEF(&_19$$5);
	ZVAL_UNDEF(&_20$$9);
	ZVAL_UNDEF(&_22$$9);
	ZVAL_UNDEF(&_23$$9);
	ZVAL_UNDEF(&_24$$11);
	ZVAL_UNDEF(&_26$$11);
	ZVAL_UNDEF(&_27$$11);
	ZVAL_UNDEF(&_28$$11);
	ZVAL_UNDEF(&_29$$13);
	ZVAL_UNDEF(&_31$$13);
	ZVAL_UNDEF(&_32$$13);
	ZVAL_UNDEF(&_33$$15);
	ZVAL_UNDEF(&_34$$15);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_17$$5);
	ZVAL_UNDEF(&_21$$9);
	ZVAL_UNDEF(&_25$$11);
	ZVAL_UNDEF(&_30$$13);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(job, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\adapter\\beanstalk\\job")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &job);



	/* try_start_1: */

		ZEPHIR_CALL_METHOD(&_0$$3, job, "getbody", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		_1$$3 = !(Z_TYPE_P(&_0$$3) == IS_OBJECT);
		if (!(_1$$3)) {
			ZEPHIR_CALL_METHOD(&_2$$3, job, "getbody", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			_1$$3 = !(zephir_instance_of_ev(&_2$$3, twistersfury_phalcon_queue_job_jobinterface_ce));
		}
		if (UNEXPECTED(_1$$3)) {
			ZEPHIR_OBS_VAR(&_3$$4);
			zephir_read_property(&_3$$4, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
			ZEPHIR_INIT_VAR(&_4$$4);
			zephir_create_array(&_4$$4, 1, 0);
			ZEPHIR_INIT_VAR(&_5$$4);
			ZEPHIR_CALL_METHOD(&_6$$4, job, "getbody", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			if (Z_TYPE_P(&_6$$4) == IS_OBJECT) {
				ZEPHIR_CALL_METHOD(&_7$$4, job, "getbody", NULL, 0);
				zephir_check_call_status_or_jump(try_end_1);
				ZEPHIR_INIT_NVAR(&_5$$4);
				zephir_get_class(&_5$$4, &_7$$4, 0);
			} else {
				ZEPHIR_CALL_METHOD(&_5$$4, job, "getbody", NULL, 0);
				zephir_check_call_status_or_jump(try_end_1);
			}
			zephir_array_update_string(&_4$$4, SL("class"), &_5$$4, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_VAR(&_8$$4);
			ZVAL_STRING(&_8$$4, "Job Does Not Implement JobInterface");
			ZEPHIR_CALL_METHOD(NULL, &_3$$4, "debug", NULL, 0, &_8$$4, &_4$$4);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_INIT_NVAR(&_8$$4);
			object_init_ex(&_8$$4, twistersfury_phalcon_queue_exceptions_fatalexception_ce);
			ZEPHIR_INIT_VAR(&_9$$4);
			ZVAL_STRING(&_9$$4, "Job Does Not Implement JobInterface");
			ZEPHIR_CALL_METHOD(NULL, &_8$$4, "__construct", NULL, 14, &_9$$4);
			zephir_check_call_status_or_jump(try_end_1);
			zephir_throw_exception_debug(&_8$$4, "twistersfury/phalcon/queue/Cli/Task/QueueTask.zep", 114);
			goto try_end_1;

		}
		ZEPHIR_OBS_VAR(&_10$$3);
		zephir_read_property(&_10$$3, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(&_11$$3);
		zephir_create_array(&_11$$3, 2, 0);
		ZEPHIR_INIT_VAR(&_12$$3);
		ZEPHIR_CALL_METHOD(&_13$$3, job, "getbody", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		zephir_get_class(&_12$$3, &_13$$3, 0);
		zephir_array_update_string(&_11$$3, SL("class"), &_12$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_CALL_METHOD(&_14$$3, job, "stats", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		zephir_array_update_string(&_11$$3, SL("stats"), &_14$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_NVAR(&_12$$3);
		ZVAL_STRING(&_12$$3, "Processing Queue Job");
		ZEPHIR_CALL_METHOD(NULL, &_10$$3, "info", NULL, 0, &_12$$3, &_11$$3);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(&_14$$3, job, "getbody", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(NULL, &_14$$3, "handle", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(NULL, job, "delete", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_15);
		ZVAL_OBJ(&_15, EG(exception));
		Z_ADDREF_P(&_15);
		if (zephir_instance_of_ev(&_15, twistersfury_phalcon_queue_exceptions_restart_ce)) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&exception, &_15);
			ZEPHIR_OBS_VAR(&_16$$5);
			zephir_read_property(&_16$$5, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
			ZEPHIR_INIT_VAR(&_17$$5);
			zephir_create_array(&_17$$5, 1, 0);
			ZEPHIR_CALL_METHOD(&_18$$5, &exception, "getmessage", NULL, 0);
			zephir_check_call_status();
			zephir_array_update_string(&_17$$5, SL("message"), &_18$$5, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_VAR(&_19$$5);
			ZVAL_STRING(&_19$$5, "Restart Initialized");
			ZEPHIR_CALL_METHOD(NULL, &_16$$5, "notice", NULL, 0, &_19$$5, &_17$$5);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, this_ptr, "registerrestart", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, job, "delete", NULL, 0);
			zephir_check_call_status();
		} else {
			if (zephir_instance_of_ev(&_15, twistersfury_phalcon_queue_exceptions_exitexception_ce)) {
				zend_clear_exception();
				ZEPHIR_CPY_WRT(&exception, &_15);
				zephir_throw_exception_debug(&exception, "twistersfury/phalcon/queue/Cli/Task/QueueTask.zep", 139);
				ZEPHIR_MM_RESTORE();
				return;
			} else {
				if (zephir_instance_of_ev(&_15, twistersfury_phalcon_queue_exceptions_fatalexception_ce)) {
					zend_clear_exception();
					ZEPHIR_CPY_WRT(&exception, &_15);
					ZEPHIR_OBS_VAR(&_20$$9);
					zephir_read_property(&_20$$9, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
					ZEPHIR_INIT_VAR(&_21$$9);
					zephir_create_array(&_21$$9, 3, 0);
					ZEPHIR_CALL_METHOD(&_22$$9, &exception, "getmessage", NULL, 0);
					zephir_check_call_status();
					zephir_array_update_string(&_21$$9, SL("message"), &_22$$9, PH_COPY | PH_SEPARATE);
					ZEPHIR_CALL_METHOD(&_22$$9, &exception, "getline", NULL, 0);
					zephir_check_call_status();
					zephir_array_update_string(&_21$$9, SL("line"), &_22$$9, PH_COPY | PH_SEPARATE);
					ZEPHIR_CALL_METHOD(&_22$$9, &exception, "getfile", NULL, 0);
					zephir_check_call_status();
					zephir_array_update_string(&_21$$9, SL("file"), &_22$$9, PH_COPY | PH_SEPARATE);
					ZEPHIR_INIT_VAR(&_23$$9);
					ZVAL_STRING(&_23$$9, "Fatal Exception");
					ZEPHIR_CALL_METHOD(NULL, &_20$$9, "error", NULL, 0, &_23$$9, &_21$$9);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(NULL, job, "delete", NULL, 0);
					zephir_check_call_status();
				} else {
					if (zephir_instance_of_ev(&_15, twistersfury_phalcon_queue_exceptions_nonfatalexception_release_ce)) {
						zend_clear_exception();
						ZEPHIR_CPY_WRT(&exception, &_15);
						ZEPHIR_OBS_VAR(&_24$$11);
						zephir_read_property(&_24$$11, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
						ZEPHIR_INIT_VAR(&_25$$11);
						zephir_create_array(&_25$$11, 2, 0);
						ZEPHIR_CALL_METHOD(&_26$$11, &exception, "getmessage", NULL, 0);
						zephir_check_call_status();
						zephir_array_update_string(&_25$$11, SL("message"), &_26$$11, PH_COPY | PH_SEPARATE);
						ZEPHIR_CALL_METHOD(&_26$$11, &exception, "getdelay", NULL, 0);
						zephir_check_call_status();
						zephir_array_update_string(&_25$$11, SL("delay"), &_26$$11, PH_COPY | PH_SEPARATE);
						ZEPHIR_INIT_VAR(&_27$$11);
						ZVAL_STRING(&_27$$11, "Releasing Job");
						ZEPHIR_CALL_METHOD(NULL, &_24$$11, "notice", NULL, 0, &_27$$11, &_25$$11);
						zephir_check_call_status();
						ZEPHIR_CALL_METHOD(&_26$$11, &exception, "getdelay", NULL, 0);
						zephir_check_call_status();
						ZVAL_LONG(&_28$$11, 100);
						ZEPHIR_CALL_METHOD(NULL, job, "release", NULL, 0, &_28$$11, &_26$$11);
						zephir_check_call_status();
					} else {
						if (zephir_instance_of_ev(&_15, twistersfury_phalcon_queue_exceptions_nonfatalexception_ce)) {
							zend_clear_exception();
							ZEPHIR_CPY_WRT(&exception, &_15);
							ZEPHIR_OBS_VAR(&_29$$13);
							zephir_read_property(&_29$$13, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
							ZEPHIR_INIT_VAR(&_30$$13);
							zephir_create_array(&_30$$13, 1, 0);
							ZEPHIR_CALL_METHOD(&_31$$13, &exception, "getmessage", NULL, 0);
							zephir_check_call_status();
							zephir_array_update_string(&_30$$13, SL("message"), &_31$$13, PH_COPY | PH_SEPARATE);
							ZEPHIR_INIT_VAR(&_32$$13);
							ZVAL_STRING(&_32$$13, "Non Fatal Exception");
							ZEPHIR_CALL_METHOD(NULL, &_29$$13, "notice", NULL, 0, &_32$$13, &_30$$13);
							zephir_check_call_status();
							ZEPHIR_CALL_METHOD(NULL, job, "release", NULL, 0);
							zephir_check_call_status();
						} else {
							if (zephir_is_instance_of(&_15, SL("Throwable"))) {
								zend_clear_exception();
								ZEPHIR_CPY_WRT(&exception, &_15);
								ZEPHIR_OBS_VAR(&_33$$15);
								zephir_read_property(&_33$$15, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
								ZEPHIR_CALL_METHOD(&_34$$15, &exception, "getmessage", NULL, 0);
								zephir_check_call_status();
								ZEPHIR_CALL_METHOD(NULL, &_33$$15, "error", NULL, 0, &_34$$15);
								zephir_check_call_status();
								ZEPHIR_CALL_METHOD(NULL, job, "delete", NULL, 0);
								zephir_check_call_status();
							}
						}
					}
				}
			}
		}
	}
	ZEPHIR_OBS_VAR(&_35);
	zephir_read_property(&_35, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_36);
	ZVAL_STRING(&_36, "Done Processing Queue Job");
	ZEPHIR_CALL_METHOD(NULL, &_35, "info", NULL, 0, &_36);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, canProcess)
{
	zval _0, _1, _2, _3, _4, _5, _6$$5, _7$$5, _8$$5, _9$$5, _10$$5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6$$5);
	ZVAL_UNDEF(&_7$$5);
	ZVAL_UNDEF(&_8$$5);
	ZVAL_UNDEF(&_9$$5);
	ZVAL_UNDEF(&_10$$5);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "cache");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_3);
	zephir_read_property(&_3, this_ptr, ZEND_STRL("cache"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getrestartkey", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, &_3, "has", NULL, 0, &_5);
	zephir_check_call_status();
	if (UNEXPECTED(!zephir_is_true(&_1))) {
		RETURN_MM_BOOL(1);
	} else if (!(zephir_is_true(&_4))) {
		RETURN_MM_BOOL(1);
	}

	/* try_start_1: */

		ZEPHIR_OBS_VAR(&_7$$5);
		zephir_read_property(&_7$$5, this_ptr, ZEND_STRL("cache"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_9$$5, this_ptr, "getrestartkey", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(&_8$$5, &_7$$5, "get", NULL, 0, &_9$$5);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(&_6$$5, this_ptr, "builddate", NULL, 0, &_8$$5);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(&_10$$5, this_ptr, "getstarttime", NULL, 0);
		zephir_check_call_status_or_jump(try_end_1);
		RETURN_MM_BOOL(ZEPHIR_LT(&_6$$5, &_10$$5));

	try_end_1:

	zend_clear_exception();
	RETURN_MM_BOOL(0);
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, buildDate)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *dateTime_param = NULL, _0, _2;
	zval dateTime;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&dateTime);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(dateTime)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &dateTime_param);
	if (UNEXPECTED(Z_TYPE_P(dateTime_param) != IS_STRING && Z_TYPE_P(dateTime_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'dateTime' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(dateTime_param) == IS_STRING)) {
		zephir_get_strval(&dateTime, dateTime_param);
	} else {
		ZEPHIR_INIT_VAR(&dateTime);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	zephir_array_fast_append(&_1, &dateTime);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "DateTime");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_2, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getRestartKey)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury.Phalcon.Queue.Task.Restart");
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, getNow)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "DateTime");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask, registerRestart)
{
	zval _4$$3;
	zval _0, _1, _2, now$$3, _3$$3, _5$$3, _7$$3, result$$3, _8$$3, _9$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_6 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&now$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&result$$3);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);
	ZVAL_UNDEF(&_4$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "cache");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	if (UNEXPECTED(zephir_is_true(&_1))) {
		ZEPHIR_CALL_METHOD(&now$$3, this_ptr, "getnow", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_OBS_VAR(&_3$$3);
		zephir_read_property(&_3$$3, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(&_4$$3);
		zephir_create_array(&_4$$3, 2, 0);
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getrestartkey", &_6, 0);
		zephir_check_call_status();
		zephir_array_update_string(&_4$$3, SL("key"), &_5$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_VAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "Y-m-d H:i:s");
		ZEPHIR_CALL_METHOD(&_5$$3, &now$$3, "format", NULL, 0, &_7$$3);
		zephir_check_call_status();
		zephir_array_update_string(&_4$$3, SL("date"), &_5$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_NVAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "Restart Cache");
		ZEPHIR_CALL_METHOD(NULL, &_3$$3, "debug", NULL, 0, &_7$$3, &_4$$3);
		zephir_check_call_status();
		ZEPHIR_OBS_VAR(&_8$$3);
		zephir_read_property(&_8$$3, this_ptr, ZEND_STRL("cache"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getrestartkey", &_6, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "Y-m-d H:i:s");
		ZEPHIR_CALL_METHOD(&_9$$3, &now$$3, "format", NULL, 0, &_7$$3);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&result$$3, &_8$$3, "set", NULL, 0, &_5$$3, &_9$$3);
		zephir_check_call_status();
		if (UNEXPECTED(ZEPHIR_IS_FALSE_IDENTICAL(&result$$3))) {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(zephir_get_internal_ce(SL("phalcon\\storage\\exception")), "Unable To Save Restart", "twistersfury/phalcon/queue/Cli/Task/QueueTask.zep", 236);
			return;
		}
	}
	RETURN_THIS();
}

