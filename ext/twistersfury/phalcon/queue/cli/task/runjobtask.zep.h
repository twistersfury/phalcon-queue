
extern zend_class_entry *twistersfury_phalcon_queue_cli_task_runjobtask_ce;

ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask);

PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, mainAction);
PHP_METHOD(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, getOptions);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_runjobtask_mainaction, 0, 0, TwistersFury\\Phalcon\\Queue\\Cli\\Task\\QueueTask, 0)
	ZEND_ARG_TYPE_INFO(0, className, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, serviceName, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_cli_task_runjobtask_getoptions, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_cli_task_runjobtask_method_entry) {
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, mainAction, arginfo_twistersfury_phalcon_queue_cli_task_runjobtask_mainaction, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask, getOptions, arginfo_twistersfury_phalcon_queue_cli_task_runjobtask_getoptions, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
