
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Exceptions_NonFatalException_Release)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Exceptions\\NonFatalException, Release, twistersfury_phalcon_queue, exceptions_nonfatalexception_release, twistersfury_phalcon_queue_exceptions_nonfatalexception_ce, twistersfury_phalcon_queue_exceptions_nonfatalexception_release_method_entry, 0);

	zend_declare_property_string(twistersfury_phalcon_queue_exceptions_nonfatalexception_release_ce, SL("message"), "Job Scheduled For Release", ZEND_ACC_PROTECTED);
	/**
	 * @var int
	 */
	zend_declare_property_long(twistersfury_phalcon_queue_exceptions_nonfatalexception_release_ce, SL("delay"), 0, ZEND_ACC_PRIVATE);
	return SUCCESS;
}

/**
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Exceptions_NonFatalException_Release, getDelay)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "delay");
}

/**
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Exceptions_NonFatalException_Release, setDelay)
{
	zval *delay_param = NULL, _0;
	zend_long delay;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(delay)
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &delay_param);
	delay = zephir_get_intval(delay_param);


	ZEPHIR_INIT_ZVAL_NREF(_0);
	ZVAL_LONG(&_0, delay);
	zephir_update_property_zval(this_ptr, ZEND_STRL("delay"), &_0);
	RETURN_THISW();
}

