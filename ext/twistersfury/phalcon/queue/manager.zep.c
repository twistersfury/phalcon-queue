
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Manager)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue, Manager, twistersfury_phalcon_queue, manager, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_phalcon_queue_manager_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, getDriver)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *queueName_param = NULL, queue, _0, _1, _2$$3;
	zval queueName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&queueName);
	ZVAL_UNDEF(&queue);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2$$3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(queueName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &queueName_param);
	if (UNEXPECTED(Z_TYPE_P(queueName_param) != IS_STRING && Z_TYPE_P(queueName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'queueName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(queueName_param) == IS_STRING)) {
		zephir_get_strval(&queueName, queueName_param);
	} else {
		ZEPHIR_INIT_VAR(&queueName);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "queue");
	ZEPHIR_CALL_METHOD(&queue, &_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &queue, "choose", NULL, 0, &queueName);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &queue, "watch", NULL, 0, &queueName);
	zephir_check_call_status();
	if (!ZEPHIR_IS_STRING_IDENTICAL(&queueName, "default")) {
		ZEPHIR_INIT_VAR(&_2$$3);
		ZVAL_STRING(&_2$$3, "default");
		ZEPHIR_CALL_METHOD(NULL, &queue, "ignore", NULL, 0, &_2$$3);
		zephir_check_call_status();
	}
	RETURN_CCTOR(&queue);
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, put)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval options;
	zval *queueName_param = NULL, *job, job_sub, *options_param = NULL, _0;
	zval queueName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&queueName);
	ZVAL_UNDEF(&job_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&options);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(queueName)
		Z_PARAM_OBJECT_OF_CLASS(job, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\job\\jobinterface")))
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY_OR_NULL(options)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &queueName_param, &job, &options_param);
	if (UNEXPECTED(Z_TYPE_P(queueName_param) != IS_STRING && Z_TYPE_P(queueName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'queueName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(queueName_param) == IS_STRING)) {
		zephir_get_strval(&queueName, queueName_param);
	} else {
		ZEPHIR_INIT_VAR(&queueName);
	}
	if (!options_param) {
		ZEPHIR_INIT_VAR(&options);
	} else {
		zephir_get_arrval(&options, options_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0, &queueName);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_0, "put", NULL, 0, job, &options);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_Phalcon_Queue_Manager, reserve)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long timeout, ZEPHIR_LAST_CALL_STATUS;
	zval *queueName_param = NULL, *timeout_param = NULL, _0, _1;
	zval queueName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&queueName);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(queueName)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(timeout)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &queueName_param, &timeout_param);
	if (UNEXPECTED(Z_TYPE_P(queueName_param) != IS_STRING && Z_TYPE_P(queueName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'queueName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(queueName_param) == IS_STRING)) {
		zephir_get_strval(&queueName, queueName_param);
	} else {
		ZEPHIR_INIT_VAR(&queueName);
	}
	if (!timeout_param) {
		timeout = 0;
	} else {
	if (UNEXPECTED(Z_TYPE_P(timeout_param) != IS_LONG)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'timeout' must be of the type int"));
		RETURN_MM_NULL();
	}
	timeout = Z_LVAL_P(timeout_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0, &queueName);
	zephir_check_call_status();
	ZVAL_LONG(&_1, timeout);
	ZEPHIR_RETURN_CALL_METHOD(&_0, "reserve", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

