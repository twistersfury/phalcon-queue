
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(twistersfury_phalcon_queue_1__closure)
{
	ZEPHIR_REGISTER_CLASS(twistersfury\\phalcon\\queue, 1__closure, twistersfury_phalcon_queue, 1__closure, twistersfury_phalcon_queue_1__closure_method_entry, ZEND_ACC_FINAL_CLASS);

	return SUCCESS;
}

PHP_METHOD(twistersfury_phalcon_queue_1__closure, __invoke)
{
	zval _2, _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *config = NULL, config_sub, __$null, _0$$3, _1$$3, queue, _4, _5, _6, _7$$4, _8$$4, _9$$4, _10$$4, _11$$5, _12$$5;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&config_sub);
	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&queue);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7$$4);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);
	ZVAL_UNDEF(&_10$$4);
	ZVAL_UNDEF(&_11$$5);
	ZVAL_UNDEF(&_12$$5);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_OBJECT_OF_CLASS_OR_NULL(config, zephir_get_internal_ce(SL("phalcon\\config\\config")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &config);
	if (!config) {
		config = &config_sub;
		config = &__$null;
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	if (Z_TYPE_P(config) == IS_NULL) {
		ZEPHIR_INIT_VAR(&_1$$3);
		ZVAL_STRING(&_1$$3, "config");
		ZEPHIR_CALL_METHOD(&_0$$3, this_ptr, "getshared", NULL, 0, &_1$$3);
		zephir_check_call_status();
		ZEPHIR_OBS_NVAR(config);
		zephir_read_property(config, &_0$$3, ZEND_STRL("queue"), PH_NOISY_CC);
	}
	ZEPHIR_INIT_VAR(&_2);
	zephir_create_array(&_2, 1, 0);
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 2, 0);
	ZEPHIR_OBS_VAR(&_4);
	zephir_read_property(&_4, config, ZEND_STRL("host"), PH_NOISY_CC);
	zephir_array_update_string(&_3, SL("host"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_OBS_NVAR(&_4);
	zephir_read_property(&_4, config, ZEND_STRL("port"), PH_NOISY_CC);
	zephir_array_update_string(&_3, SL("port"), &_4, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_2, &_3);
	ZEPHIR_INIT_VAR(&_5);
	ZVAL_STRING(&_5, "TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk");
	ZEPHIR_CALL_METHOD(&queue, this_ptr, "get", NULL, 0, &_5, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_5);
	ZVAL_STRING(&_5, "tube");
	ZEPHIR_CALL_METHOD(&_6, config, "has", NULL, 0, &_5);
	zephir_check_call_status();
	if (zephir_is_true(&_6)) {
		zephir_read_property(&_7$$4, config, ZEND_STRL("tube"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(NULL, &queue, "choose", NULL, 0, &_7$$4);
		zephir_check_call_status();
		zephir_read_property(&_8$$4, config, ZEND_STRL("tube"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(NULL, &queue, "watch", NULL, 0, &_8$$4);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_9$$4);
		ZVAL_STRING(&_9$$4, "default");
		ZEPHIR_CALL_METHOD(NULL, &queue, "ignore", NULL, 0, &_9$$4);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_9$$4);
		ZVAL_STRING(&_9$$4, "keepAlive");
		ZEPHIR_CALL_METHOD(&_10$$4, config, "has", NULL, 0, &_9$$4);
		zephir_check_call_status();
		if (zephir_is_true(&_10$$4)) {
			ZEPHIR_INIT_VAR(&_12$$5);
			ZVAL_STRING(&_12$$5, "TwistersFury\\Phalcon\\Queue\\Job\\KeepAlive");
			ZEPHIR_CALL_METHOD(&_11$$5, this_ptr, "get", NULL, 0, &_12$$5);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, &queue, "put", NULL, 0, &_11$$5);
			zephir_check_call_status();
		}
	}
	RETURN_CCTOR(&queue);
}

