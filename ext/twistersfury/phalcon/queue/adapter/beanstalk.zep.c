
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/fcall.h"
#include "kernel/exception.h"
#include "kernel/string.h"
#include "kernel/concat.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/file.h"


/**
 * Phalcon\Queue\Beanstalk
 *
 * Class to access the beanstalk queue service.
 * Partially implements the protocol version 1.2
 *
 * <code>
 * use Phalcon\Queue\Beanstalk;
 *
 * $queue = new Beanstalk(
 *     [
 *         "host"       => "127.0.0.1",
 *         "port"       => 11300,
 *         "persistent" => true,
 *     ]
 * );
 * </code>
 *
 * @link http://www.igvita.com/2010/05/20/scalable-work-queues-with-beanstalk/
 */
ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Adapter_Beanstalk)
{
	ZEPHIR_REGISTER_CLASS(TwistersFury\\Phalcon\\Queue\\Adapter, Beanstalk, twistersfury_phalcon_queue, adapter_beanstalk, twistersfury_phalcon_queue_adapter_beanstalk_method_entry, 0);

	/**
	 * Connection resource
	 * @var resource
	 */
	zend_declare_property_null(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("_connection"), ZEND_ACC_PROTECTED);
	/**
	 * Connection options
	 * @var array
	 */
	zend_declare_property_null(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("_parameters"), ZEND_ACC_PROTECTED);
	/**
	 * Seconds to wait before putting the job in the ready queue.
	 * The job will be in the "delayed" state during this time.
	 *
	 * @const integer
	 */
	zephir_declare_class_constant_long(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_DELAY"), 0);

	/**
	 * Jobs with smaller priority values will be scheduled before jobs with larger priorities.
	 * The most urgent priority is 0, the least urgent priority is 4294967295.
	 *
	 * @const integer
	 */
	zephir_declare_class_constant_long(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_PRIORITY"), 100);

	/**
	 * Time to run - number of seconds to allow a worker to run this job.
	 * The minimum ttr is 1.
	 *
	 * @const integer
	 */
	zephir_declare_class_constant_long(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_TTR"), 86400);

	/**
	 * Default tube name
	 * @const string
	 */
	zephir_declare_class_constant_string(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_TUBE"), "default");

	/**
	 * Default connected host
	 * @const string
	 */
	zephir_declare_class_constant_string(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_HOST"), "127.0.0.1");

	/**
	 * Default connected port
	 * @const integer
	 */
	zephir_declare_class_constant_long(twistersfury_phalcon_queue_adapter_beanstalk_ce, SL("DEFAULT_PORT"), 11300);

	return SUCCESS;
}

/**
 * Phalcon\Queue\Beanstalk
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, __construct)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *parameters_param = NULL, __$false, _0$$3, _1$$4;
	zval parameters;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&parameters);
	ZVAL_BOOL(&__$false, 0);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_1$$4);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(parameters)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &parameters_param);
	if (!parameters_param) {
		ZEPHIR_INIT_VAR(&parameters);
		array_init(&parameters);
	} else {
		zephir_get_arrval(&parameters, parameters_param);
	}


	if (!(zephir_array_isset_string(&parameters, SL("host")))) {
		ZEPHIR_INIT_VAR(&_0$$3);
		ZVAL_STRING(&_0$$3, "127.0.0.1");
		zephir_array_update_string(&parameters, SL("host"), &_0$$3, PH_COPY | PH_SEPARATE);
	}
	if (!(zephir_array_isset_string(&parameters, SL("port")))) {
		ZEPHIR_INIT_VAR(&_1$$4);
		ZVAL_LONG(&_1$$4, 11300);
		zephir_array_update_string(&parameters, SL("port"), &_1$$4, PH_COPY | PH_SEPARATE);
	}
	if (!(zephir_array_isset_string(&parameters, SL("persistent")))) {
		zephir_array_update_string(&parameters, SL("persistent"), &__$false, PH_COPY | PH_SEPARATE);
	}
	zephir_update_property_zval(this_ptr, ZEND_STRL("_parameters"), &parameters);
	ZEPHIR_MM_RESTORE();
}

/**
 * Makes a connection to the Beanstalkd server
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, connect)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval __$null, connection, parameters, _0, _9, _1$$4, _2$$4, _3$$4, _4$$4, _5$$5, _6$$5, _7$$5, _8$$5;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&connection);
	ZVAL_UNDEF(&parameters);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_1$$4);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_5$$5);
	ZVAL_UNDEF(&_6$$5);
	ZVAL_UNDEF(&_7$$5);
	ZVAL_UNDEF(&_8$$5);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&connection);
	zephir_read_property(&connection, this_ptr, ZEND_STRL("_connection"), PH_NOISY_CC);
	if (Z_TYPE_P(&connection) == IS_RESOURCE) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "disconnect", NULL, 0);
		zephir_check_call_status();
	}
	ZEPHIR_OBS_VAR(&parameters);
	zephir_read_property(&parameters, this_ptr, ZEND_STRL("_parameters"), PH_NOISY_CC);
	zephir_array_fetch_string(&_0, &parameters, SL("persistent"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 139);
	if (zephir_is_true(&_0)) {
		zephir_array_fetch_string(&_1$$4, &parameters, SL("host"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 140);
		zephir_array_fetch_string(&_2$$4, &parameters, SL("port"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 140);
		ZVAL_NULL(&_3$$4);
		ZVAL_NULL(&_4$$4);
		ZEPHIR_MAKE_REF(&_3$$4);
		ZEPHIR_MAKE_REF(&_4$$4);
		ZEPHIR_CALL_FUNCTION(&connection, "pfsockopen", NULL, 1, &_1$$4, &_2$$4, &_3$$4, &_4$$4);
		ZEPHIR_UNREF(&_3$$4);
		ZEPHIR_UNREF(&_4$$4);
		zephir_check_call_status();
	} else {
		zephir_array_fetch_string(&_5$$5, &parameters, SL("host"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 142);
		zephir_array_fetch_string(&_6$$5, &parameters, SL("port"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 142);
		ZVAL_NULL(&_7$$5);
		ZVAL_NULL(&_8$$5);
		ZEPHIR_MAKE_REF(&_7$$5);
		ZEPHIR_MAKE_REF(&_8$$5);
		ZEPHIR_CALL_FUNCTION(&connection, "fsockopen", NULL, 2, &_5$$5, &_6$$5, &_7$$5, &_8$$5);
		ZEPHIR_UNREF(&_7$$5);
		ZEPHIR_UNREF(&_8$$5);
		zephir_check_call_status();
	}
	if (Z_TYPE_P(&connection) != IS_RESOURCE) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "Can't connect to Beanstalk server", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 146);
		return;
	}
	ZVAL_LONG(&_9, -1);
	ZEPHIR_CALL_FUNCTION(NULL, "stream_set_timeout", NULL, 3, &connection, &_9, &__$null);
	zephir_check_call_status();
	zephir_update_property_zval(this_ptr, ZEND_STRL("_connection"), &connection);
	RETURN_CCTOR(&connection);
}

/**
 * Puts a job on the queue using specified tube.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, put)
{
	zend_bool _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval options;
	zval *data, data_sub, *options_param = NULL, priority, delay, ttr, serialized, response, status, length, _0, _2;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&data_sub);
	ZVAL_UNDEF(&priority);
	ZVAL_UNDEF(&delay);
	ZVAL_UNDEF(&ttr);
	ZVAL_UNDEF(&serialized);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&status);
	ZVAL_UNDEF(&length);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&options);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_ZVAL(data)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY_OR_NULL(options)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &data, &options_param);
	if (!options_param) {
		ZEPHIR_INIT_VAR(&options);
	} else {
		zephir_get_arrval(&options, options_param);
	}


	ZEPHIR_OBS_VAR(&priority);
	if (!(zephir_array_isset_string_fetch(&priority, &options, SL("priority"), 0))) {
		ZEPHIR_INIT_NVAR(&priority);
		ZVAL_LONG(&priority, 100);
	}
	ZEPHIR_OBS_VAR(&delay);
	if (!(zephir_array_isset_string_fetch(&delay, &options, SL("delay"), 0))) {
		ZEPHIR_INIT_NVAR(&delay);
		ZVAL_LONG(&delay, 0);
	}
	ZEPHIR_OBS_VAR(&ttr);
	if (!(zephir_array_isset_string_fetch(&ttr, &options, SL("ttr"), 0))) {
		ZEPHIR_INIT_NVAR(&ttr);
		ZVAL_LONG(&ttr, 86400);
	}
	ZEPHIR_CALL_FUNCTION(&serialized, "serialize", NULL, 4, data);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&length);
	ZVAL_LONG(&length, zephir_fast_strlen_ev(&serialized));
	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_SVSVSVSVSV(&_0, "put ", &priority, " ", &delay, " ", &ttr, " ", &length, "\r\n", &serialized);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&status, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 190);
	_1 = !ZEPHIR_IS_STRING(&status, "INSERTED");
	if (_1) {
		_1 = !ZEPHIR_IS_STRING(&status, "BURIED");
	}
	if (_1) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_OBS_VAR(&_2);
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 196);
	RETURN_MM_LONG(zephir_get_intval(&_2));
}

/**
 * Reserves/locks a ready job from the specified tube.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, reserve)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *timeout = NULL, timeout_sub, __$null, command, response, _0, _1, _2, _3, _4;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&timeout_sub);
	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&command);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL_OR_NULL(timeout)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &timeout);
	if (!timeout) {
		timeout = &timeout_sub;
		timeout = &__$null;
	}


	ZEPHIR_INIT_VAR(&command);
	if (Z_TYPE_P(timeout) != IS_NULL) {
		ZEPHIR_CONCAT_SV(&command, "reserve-with-timeout ", timeout);
	} else {
		ZVAL_STRING(&command, "reserve");
	}
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &command);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_0, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 215);
	if (!ZEPHIR_IS_STRING(&_0, "RESERVED")) {
		RETURN_MM_BOOL(0);
	}
	object_init_ex(return_value, twistersfury_phalcon_queue_adapter_beanstalk_job_ce);
	zephir_array_fetch_long(&_1, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 225);
	zephir_array_fetch_long(&_3, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 225);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "read", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_4, "unserialize", NULL, 6, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, 7, this_ptr, &_1, &_4);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * Change the active tube. By default the tube is "default".
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, choose)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *tube_param = NULL, response, _1, _2;
	zval tube, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tube);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tube)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tube_param);
	if (UNEXPECTED(Z_TYPE_P(tube_param) != IS_STRING && Z_TYPE_P(tube_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'tube' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(tube_param) == IS_STRING)) {
		zephir_get_strval(&tube, tube_param);
	} else {
		ZEPHIR_INIT_VAR(&tube);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_SV(&_0, "use ", &tube);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 238);
	if (!ZEPHIR_IS_STRING(&_1, "USING")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 242);
	RETURN_CTOR(&_2);
}

/**
 * The watch command adds the named tube to the watch list for the current connection.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, watch)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *tube_param = NULL, response, _1, _2;
	zval tube, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tube);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tube)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tube_param);
	if (UNEXPECTED(Z_TYPE_P(tube_param) != IS_STRING && Z_TYPE_P(tube_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'tube' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(tube_param) == IS_STRING)) {
		zephir_get_strval(&tube, tube_param);
	} else {
		ZEPHIR_INIT_VAR(&tube);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_SV(&_0, "watch ", &tube);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 255);
	if (!ZEPHIR_IS_STRING(&_1, "WATCHING")) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_OBS_VAR(&_2);
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 259);
	RETURN_MM_LONG(zephir_get_intval(&_2));
}

/**
 * It removes the named tube from the watch list for the current connection.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, ignore)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *tube_param = NULL, response, _1, _2;
	zval tube, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tube);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tube)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tube_param);
	if (UNEXPECTED(Z_TYPE_P(tube_param) != IS_STRING && Z_TYPE_P(tube_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'tube' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(tube_param) == IS_STRING)) {
		zephir_get_strval(&tube, tube_param);
	} else {
		ZEPHIR_INIT_VAR(&tube);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_SV(&_0, "ignore ", &tube);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 272);
	if (!ZEPHIR_IS_STRING(&_1, "WATCHING")) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_OBS_VAR(&_2);
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 276);
	RETURN_MM_LONG(zephir_get_intval(&_2));
}

/**
 * Can delay any new job being reserved for a given time.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, pauseTube)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long delay, ZEPHIR_LAST_CALL_STATUS;
	zval *tube_param = NULL, *delay_param = NULL, response, _0, _2;
	zval tube, _1;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tube);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(tube)
		Z_PARAM_LONG(delay)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &tube_param, &delay_param);
	if (UNEXPECTED(Z_TYPE_P(tube_param) != IS_STRING && Z_TYPE_P(tube_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'tube' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(tube_param) == IS_STRING)) {
		zephir_get_strval(&tube, tube_param);
	} else {
		ZEPHIR_INIT_VAR(&tube);
	}
	delay = zephir_get_intval(delay_param);


	ZEPHIR_INIT_VAR(&_0);
	ZVAL_LONG(&_0, delay);
	ZEPHIR_INIT_VAR(&_1);
	ZEPHIR_CONCAT_SVSV(&_1, "pause-tube ", &tube, " ", &_0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_2, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 289);
	if (!ZEPHIR_IS_STRING(&_2, "PAUSED")) {
		RETURN_MM_BOOL(0);
	}
	RETURN_MM_BOOL(1);
}

/**
 * The kick command applies only to the currently used tube.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, kick)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *bound_param = NULL, response, _0, _2, _3;
	zend_long bound, ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(bound)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &bound_param);
	bound = zephir_get_intval(bound_param);


	ZEPHIR_INIT_VAR(&_0);
	ZVAL_LONG(&_0, bound);
	ZEPHIR_INIT_VAR(&_1);
	ZEPHIR_CONCAT_SV(&_1, "kick ", &_0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_2, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 306);
	if (!ZEPHIR_IS_STRING(&_2, "KICKED")) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_OBS_VAR(&_3);
	zephir_array_fetch_long(&_3, &response, 1, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 310);
	RETURN_MM_LONG(zephir_get_intval(&_3));
}

/**
 * Gives statistical information about the system as a whole.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, stats)
{
	zval response, _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "stats");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readyaml", NULL, 8);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 323);
	if (!ZEPHIR_IS_STRING(&_1, "OK")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 327);
	RETURN_CTOR(&_2);
}

/**
 * Gives statistical information about the specified tube if it exists.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, statsTube)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *tube_param = NULL, response, _1, _2;
	zval tube, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tube);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tube)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tube_param);
	if (UNEXPECTED(Z_TYPE_P(tube_param) != IS_STRING && Z_TYPE_P(tube_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'tube' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(tube_param) == IS_STRING)) {
		zephir_get_strval(&tube, tube_param);
	} else {
		ZEPHIR_INIT_VAR(&tube);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_SV(&_0, "stats-tube ", &tube);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readyaml", NULL, 8);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 340);
	if (!ZEPHIR_IS_STRING(&_1, "OK")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 344);
	RETURN_CTOR(&_2);
}

/**
 * Returns a list of all existing tubes.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubes)
{
	zval response, _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "list-tubes");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readyaml", NULL, 8);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 357);
	if (!ZEPHIR_IS_STRING(&_1, "OK")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 361);
	RETURN_CTOR(&_2);
}

/**
 * Returns the tube currently being used by the client.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubeUsed)
{
	zval response, _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "list-tube-used");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 374);
	if (!ZEPHIR_IS_STRING(&_1, "USING")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 378);
	RETURN_CTOR(&_2);
}

/**
 * Returns a list tubes currently being watched by the client.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubesWatched)
{
	zval response, _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "list-tubes-watched");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readyaml", NULL, 8);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 391);
	if (!ZEPHIR_IS_STRING(&_1, "OK")) {
		RETURN_MM_BOOL(0);
	}
	zephir_array_fetch_long(&_2, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 395);
	RETURN_CTOR(&_2);
}

/**
 * Inspect the next ready job.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekReady)
{
	zval response, _0, _1, _2, _3, _4, _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "peek-ready");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 408);
	if (!ZEPHIR_IS_STRING(&_1, "FOUND")) {
		RETURN_MM_BOOL(0);
	}
	object_init_ex(return_value, twistersfury_phalcon_queue_adapter_beanstalk_job_ce);
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 412);
	zephir_array_fetch_long(&_4, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 412);
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "read", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_5, "unserialize", NULL, 6, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, 7, this_ptr, &_2, &_5);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * Return the next job in the list of buried jobs.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekBuried)
{
	zval response, _0, _1, _2, _3, _4, _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "peek-buried");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_1, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 425);
	if (!ZEPHIR_IS_STRING(&_1, "FOUND")) {
		RETURN_MM_BOOL(0);
	}
	object_init_ex(return_value, twistersfury_phalcon_queue_adapter_beanstalk_job_ce);
	zephir_array_fetch_long(&_2, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 429);
	zephir_array_fetch_long(&_4, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 429);
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "read", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_5, "unserialize", NULL, 6, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, 7, this_ptr, &_2, &_5);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * Return the next job in the list of buried jobs.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekDelayed)
{
	zval response, _0, _1, _2, _3, _4, _5, _6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "peek-delayed");
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "write", NULL, 0, &_1);
	zephir_check_call_status();
	if (!(zephir_is_true(&_0))) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_2, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 444);
	if (!ZEPHIR_IS_STRING(&_2, "FOUND")) {
		RETURN_MM_BOOL(0);
	}
	object_init_ex(return_value, twistersfury_phalcon_queue_adapter_beanstalk_job_ce);
	zephir_array_fetch_long(&_3, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 448);
	zephir_array_fetch_long(&_5, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 448);
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "read", NULL, 0, &_5);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_6, "unserialize", NULL, 6, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, 7, this_ptr, &_3, &_6);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * The peek commands let the client inspect a job in the system.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, jobPeek)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *id_param = NULL, response, _0, _2, _3, _4, _5, _6;
	zend_long id, ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(id)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &id_param);
	id = zephir_get_intval(id_param);


	ZEPHIR_INIT_VAR(&_0);
	ZVAL_LONG(&_0, id);
	ZEPHIR_INIT_VAR(&_1);
	ZEPHIR_CONCAT_SV(&_1, "peek ", &_0);
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	zephir_array_fetch_long(&_2, &response, 0, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 462);
	if (!ZEPHIR_IS_STRING(&_2, "FOUND")) {
		RETURN_MM_BOOL(0);
	}
	object_init_ex(return_value, twistersfury_phalcon_queue_adapter_beanstalk_job_ce);
	zephir_array_fetch_long(&_3, &response, 1, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 466);
	zephir_array_fetch_long(&_5, &response, 2, PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 466);
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "read", NULL, 0, &_5);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_6, "unserialize", NULL, 6, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, return_value, "__construct", NULL, 7, this_ptr, &_3, &_6);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * Reads the latest status from the Beanstalkd server
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readStatus)
{
	zval status;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&status);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&status, this_ptr, "read", NULL, 0);
	zephir_check_call_status();
	if (ZEPHIR_IS_FALSE_IDENTICAL(&status)) {
		array_init(return_value);
		RETURN_MM();
	}
	zephir_fast_explode_str(return_value, SL(" "), &status, LONG_MAX);
	RETURN_MM();
}

/**
 * Fetch a YAML payload from the Beanstalkd server
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readYaml)
{
	zval response, status, numberOfBytes, data;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&status);
	ZVAL_UNDEF(&numberOfBytes);
	ZVAL_UNDEF(&data);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&response, this_ptr, "readstatus", NULL, 5);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&status);
	zephir_array_fetch_long(&status, &response, 0, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 491);
	if (zephir_fast_count_int(&response) > 1) {
		ZEPHIR_OBS_VAR(&numberOfBytes);
		zephir_array_fetch_long(&numberOfBytes, &response, 1, PH_NOISY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 494);
		ZEPHIR_CALL_METHOD(&response, this_ptr, "read", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(&data, "yaml_parse", NULL, 9, &response);
		zephir_check_call_status();
	} else {
		ZEPHIR_INIT_NVAR(&numberOfBytes);
		ZVAL_LONG(&numberOfBytes, 0);
		ZEPHIR_INIT_NVAR(&data);
		array_init(&data);
	}
	zephir_create_array(return_value, 3, 0);
	zephir_array_fast_append(return_value, &status);
	zephir_array_fast_append(return_value, &numberOfBytes);
	zephir_array_fast_append(return_value, &data);
	RETURN_MM();
}

/**
 * Reads a packet from the socket. Prior to reading from the socket will
 * check for availability of the connection.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, read)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_2 = NULL;
	zval *length_param = NULL, connection, data, _0$$5, _1$$5, _3$$5, _4$$5, _5$$5, _6$$8, _7$$8;
	zend_long length, ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
	ZVAL_UNDEF(&data);
	ZVAL_UNDEF(&_0$$5);
	ZVAL_UNDEF(&_1$$5);
	ZVAL_UNDEF(&_3$$5);
	ZVAL_UNDEF(&_4$$5);
	ZVAL_UNDEF(&_5$$5);
	ZVAL_UNDEF(&_6$$8);
	ZVAL_UNDEF(&_7$$8);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(length)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &length_param);
	if (!length_param) {
		length = 0;
	} else {
		length = zephir_get_intval(length_param);
	}


	ZEPHIR_OBS_VAR(&connection);
	zephir_read_property(&connection, this_ptr, ZEND_STRL("_connection"), PH_NOISY_CC);
	if (Z_TYPE_P(&connection) != IS_RESOURCE) {
		ZEPHIR_CALL_METHOD(&connection, this_ptr, "connect", NULL, 0);
		zephir_check_call_status();
		if (Z_TYPE_P(&connection) != IS_RESOURCE) {
			RETURN_MM_BOOL(0);
		}
	}
	if (length) {
		if (zephir_feof(&connection)) {
			RETURN_MM_BOOL(0);
		}
		ZVAL_LONG(&_0$$5, (length + 2));
		ZEPHIR_CALL_FUNCTION(&_1$$5, "stream_get_line", &_2, 10, &connection, &_0$$5);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_3$$5);
		ZVAL_STRING(&_3$$5, "\r\n");
		ZEPHIR_INIT_VAR(&data);
		zephir_fast_trim(&data, &_1$$5, &_3$$5, ZEPHIR_TRIM_RIGHT);
		ZEPHIR_CALL_FUNCTION(&_4$$5, "stream_get_meta_data", NULL, 11, &connection);
		zephir_check_call_status();
		zephir_array_fetch_string(&_5$$5, &_4$$5, SL("timed_out"), PH_NOISY | PH_READONLY, "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 535);
		if (zephir_is_true(&_5$$5)) {
			ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "Connection timed out", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 536);
			return;
		}
	} else {
		ZVAL_LONG(&_6$$8, 16384);
		ZEPHIR_INIT_VAR(&_7$$8);
		ZVAL_STRING(&_7$$8, "\r\n");
		ZEPHIR_CALL_FUNCTION(&data, "stream_get_line", &_2, 10, &connection, &_6$$8, &_7$$8);
		zephir_check_call_status();
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&data, "UNKNOWN_COMMAND")) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "UNKNOWN_COMMAND", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 544);
		return;
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&data, "JOB_TOO_BIG")) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "JOB_TOO_BIG", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 548);
		return;
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&data, "BAD_FORMAT")) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "BAD_FORMAT", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 552);
		return;
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&data, "OUT_OF_MEMORY")) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(twistersfury_phalcon_queue_adapter_beanstalk_exception_ce, "OUT_OF_MEMORY", "twistersfury/phalcon/queue/Adapter/Beanstalk.zep", 556);
		return;
	}
	RETURN_CCTOR(&data);
}

/**
 * Writes data to the socket. Performs a connection if none is available
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, write)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *data_param = NULL, connection, packet, _1;
	zval data, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&data);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&connection);
	ZVAL_UNDEF(&packet);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &data_param);
	zephir_get_strval(&data, data_param);


	ZEPHIR_OBS_VAR(&connection);
	zephir_read_property(&connection, this_ptr, ZEND_STRL("_connection"), PH_NOISY_CC);
	if (Z_TYPE_P(&connection) != IS_RESOURCE) {
		ZEPHIR_CALL_METHOD(&connection, this_ptr, "connect", NULL, 0);
		zephir_check_call_status();
		if (Z_TYPE_P(&connection) != IS_RESOURCE) {
			RETURN_MM_BOOL(0);
		}
	}
	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CONCAT_VS(&_0, &data, "\r\n");
	ZEPHIR_CPY_WRT(&packet, &_0);
	ZVAL_LONG(&_1, zephir_fast_strlen_ev(&packet));
	ZEPHIR_RETURN_CALL_FUNCTION("fwrite", NULL, 12, &connection, &packet, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

/**
 * Closes the connection to the beanstalk server.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, disconnect)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval __$null, connection;
	zval *this_ptr = getThis();

	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&connection);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&connection);
	zephir_read_property(&connection, this_ptr, ZEND_STRL("_connection"), PH_NOISY_CC);
	if (Z_TYPE_P(&connection) != IS_RESOURCE) {
		RETURN_MM_BOOL(0);
	}
	zephir_fclose(&connection);
	zephir_update_property_zval(this_ptr, ZEND_STRL("_connection"), &__$null);
	RETURN_MM_BOOL(1);
}

/**
 * Simply closes the connection.
 */
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, quit)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "quit");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "write", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "disconnect", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, this_ptr, ZEND_STRL("_connection"), PH_NOISY_CC);
	RETURN_MM_BOOL(Z_TYPE_P(&_1) != IS_RESOURCE);
}

