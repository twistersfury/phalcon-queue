
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "ext/spl/spl_exceptions.h"


/**
 * Phalcon\Queue\Beanstalk\Exception
 *
 * Exceptions thrown in Phalcon\Queue\Beanstalk will use this class
 */
ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Adapter_Beanstalk_Exception)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk, Exception, twistersfury_phalcon_queue, adapter_beanstalk_exception, spl_ce_RuntimeException, NULL, 0);

	return SUCCESS;
}

