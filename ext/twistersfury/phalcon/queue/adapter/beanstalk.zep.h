
extern zend_class_entry *twistersfury_phalcon_queue_adapter_beanstalk_ce;

ZEPHIR_INIT_CLASS(TwistersFury_Phalcon_Queue_Adapter_Beanstalk);

PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, __construct);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, connect);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, put);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, reserve);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, choose);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, watch);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, ignore);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, pauseTube);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, kick);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, stats);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, statsTube);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubes);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubeUsed);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubesWatched);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekReady);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekBuried);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekDelayed);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, jobPeek);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readStatus);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readYaml);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, read);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, write);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, disconnect);
PHP_METHOD(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, quit);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk___construct, 0, 0, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, parameters, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, parameters, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_connect, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_put, 0, 0, 1)
	ZEND_ARG_INFO(0, data)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, options, IS_ARRAY, 1, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, options, 1)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_reserve, 0, 0, 0)
	ZEND_ARG_INFO(0, timeout)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_choose, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, tube, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_watch, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, tube, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_ignore, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, tube, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_pausetube, 0, 2, _IS_BOOL, 0)
	ZEND_ARG_TYPE_INFO(0, tube, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, delay, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_kick, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, bound, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_stats, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_statstube, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, tube, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubes, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubeused, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubeswatched, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekready, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekburied, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekdelayed, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_jobpeek, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, id, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_readstatus, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_readyaml, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_read, 0, 0, 0)
	ZEND_ARG_TYPE_INFO(0, length, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_write, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, data, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_disconnect, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_phalcon_queue_adapter_beanstalk_quit, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_phalcon_queue_adapter_beanstalk_method_entry) {
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, __construct, arginfo_twistersfury_phalcon_queue_adapter_beanstalk___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, connect, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_connect, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, connect, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, put, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_put, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, reserve, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_reserve, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, choose, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_choose, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, watch, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_watch, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, ignore, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_ignore, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, pauseTube, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_pausetube, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, kick, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_kick, ZEND_ACC_PUBLIC)
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, stats, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_stats, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, stats, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, statsTube, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_statstube, ZEND_ACC_PUBLIC)
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubes, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubes, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubes, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubeUsed, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubeused, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubeUsed, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubesWatched, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_listtubeswatched, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, listTubesWatched, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekReady, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekready, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekReady, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekBuried, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekburied, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekBuried, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekDelayed, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_peekdelayed, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, peekDelayed, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, jobPeek, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_jobpeek, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readStatus, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_readstatus, ZEND_ACC_FINAL|ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, readYaml, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_readyaml, ZEND_ACC_FINAL|ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, read, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_read, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, write, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_write, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, disconnect, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_disconnect, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_Phalcon_Queue_Adapter_Beanstalk, quit, arginfo_twistersfury_phalcon_queue_adapter_beanstalk_quit, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
