
/* This file was generated automatically by Zephir do not modify it! */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#include "php_ext.h"
#include "tf_queue.h"

#include <ext/standard/info.h>

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/globals.h"
#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"



zend_class_entry *twistersfury_phalcon_queue_job_jobinterface_ce;
zend_class_entry *twistersfury_phalcon_queue_job_abstractjob_ce;
zend_class_entry *twistersfury_phalcon_queue_cli_task_runjobtask_ce;
zend_class_entry *twistersfury_phalcon_queue_exceptions_nonfatalexception_ce;
zend_class_entry *twistersfury_phalcon_queue_0__closure_ce;
zend_class_entry *twistersfury_phalcon_queue_1__closure_ce;
zend_class_entry *twistersfury_phalcon_queue_adapter_beanstalk_ce;
zend_class_entry *twistersfury_phalcon_queue_adapter_beanstalk_exception_ce;
zend_class_entry *twistersfury_phalcon_queue_adapter_beanstalk_job_ce;
zend_class_entry *twistersfury_phalcon_queue_cli_task_queuetask_ce;
zend_class_entry *twistersfury_phalcon_queue_cli_task_runjob_exittask_ce;
zend_class_entry *twistersfury_phalcon_queue_di_serviceprovider_queue_ce;
zend_class_entry *twistersfury_phalcon_queue_exceptions_exitexception_ce;
zend_class_entry *twistersfury_phalcon_queue_exceptions_fatalexception_ce;
zend_class_entry *twistersfury_phalcon_queue_exceptions_nonfatalexception_release_ce;
zend_class_entry *twistersfury_phalcon_queue_exceptions_restart_ce;
zend_class_entry *twistersfury_phalcon_queue_job_exitjob_ce;
zend_class_entry *twistersfury_phalcon_queue_job_keepalive_ce;
zend_class_entry *twistersfury_phalcon_queue_job_restart_ce;
zend_class_entry *twistersfury_phalcon_queue_manager_ce;

ZEND_DECLARE_MODULE_GLOBALS(tf_queue)

PHP_INI_BEGIN()
	STD_PHP_INI_BOOLEAN("tf_queue.debug.mode", "0", PHP_INI_ALL, OnUpdateBool, debug.mode, zend_tf_queue_globals, tf_queue_globals)
PHP_INI_END()

static PHP_MINIT_FUNCTION(tf_queue)
{
	REGISTER_INI_ENTRIES();
	zephir_module_init();
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Job_JobInterface);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Job_AbstractJob);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Cli_Task_RunJobTask);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Exceptions_NonFatalException);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Adapter_Beanstalk);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Adapter_Beanstalk_Exception);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Adapter_Beanstalk_Job);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Cli_Task_QueueTask);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Cli_Task_RunJob_ExitTask);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Di_ServiceProvider_Queue);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Exceptions_ExitException);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Exceptions_FatalException);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Exceptions_NonFatalException_Release);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Exceptions_Restart);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Job_ExitJob);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Job_KeepAlive);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Job_Restart);
	ZEPHIR_INIT(TwistersFury_Phalcon_Queue_Manager);
	ZEPHIR_INIT(twistersfury_phalcon_queue_0__closure);
	ZEPHIR_INIT(twistersfury_phalcon_queue_1__closure);
	
	return SUCCESS;
}

#ifndef ZEPHIR_RELEASE
static PHP_MSHUTDOWN_FUNCTION(tf_queue)
{
	
	zephir_deinitialize_memory();
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
#endif

/**
 * Initialize globals on each request or each thread started
 */
static void php_zephir_init_globals(zend_tf_queue_globals *tf_queue_globals)
{
	tf_queue_globals->initialized = 0;

	/* Cache Enabled */
	tf_queue_globals->cache_enabled = 1;

	/* Recursive Lock */
	tf_queue_globals->recursive_lock = 0;

	/* Static cache */
	memset(tf_queue_globals->scache, '\0', sizeof(zephir_fcall_cache_entry*) * ZEPHIR_MAX_CACHE_SLOTS);

	
	
}

/**
 * Initialize globals only on each thread started
 */
static void php_zephir_init_module_globals(zend_tf_queue_globals *tf_queue_globals)
{
	
}

static PHP_RINIT_FUNCTION(tf_queue)
{
	zend_tf_queue_globals *tf_queue_globals_ptr;
	tf_queue_globals_ptr = ZEPHIR_VGLOBAL;

	php_zephir_init_globals(tf_queue_globals_ptr);
	zephir_initialize_memory(tf_queue_globals_ptr);

	
	return SUCCESS;
}

static PHP_RSHUTDOWN_FUNCTION(tf_queue)
{
	
	zephir_deinitialize_memory();
	return SUCCESS;
}



static PHP_MINFO_FUNCTION(tf_queue)
{
	php_info_print_box_start(0);
	php_printf("%s", PHP_TF_QUEUE_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, PHP_TF_QUEUE_NAME, "enabled");
	php_info_print_table_row(2, "Author", PHP_TF_QUEUE_AUTHOR);
	php_info_print_table_row(2, "Version", PHP_TF_QUEUE_VERSION);
	php_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__ );
	php_info_print_table_row(2, "Powered by Zephir", "Version " PHP_TF_QUEUE_ZEPVERSION);
	php_info_print_table_end();
	
	DISPLAY_INI_ENTRIES();
}

static PHP_GINIT_FUNCTION(tf_queue)
{
#if defined(COMPILE_DL_TF_QUEUE) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	php_zephir_init_globals(tf_queue_globals);
	php_zephir_init_module_globals(tf_queue_globals);
}

static PHP_GSHUTDOWN_FUNCTION(tf_queue)
{
	
}


zend_function_entry php_tf_queue_functions[] = {
	ZEND_FE_END

};

static const zend_module_dep php_tf_queue_deps[] = {
	ZEND_MOD_REQUIRED("phalcon")
	ZEND_MOD_REQUIRED("tf_shared")
	ZEND_MOD_END
};

zend_module_entry tf_queue_module_entry = {
	STANDARD_MODULE_HEADER_EX,
	NULL,
	php_tf_queue_deps,
	PHP_TF_QUEUE_EXTNAME,
	php_tf_queue_functions,
	PHP_MINIT(tf_queue),
#ifndef ZEPHIR_RELEASE
	PHP_MSHUTDOWN(tf_queue),
#else
	NULL,
#endif
	PHP_RINIT(tf_queue),
	PHP_RSHUTDOWN(tf_queue),
	PHP_MINFO(tf_queue),
	PHP_TF_QUEUE_VERSION,
	ZEND_MODULE_GLOBALS(tf_queue),
	PHP_GINIT(tf_queue),
	PHP_GSHUTDOWN(tf_queue),
#ifdef ZEPHIR_POST_REQUEST
	PHP_PRSHUTDOWN(tf_queue),
#else
	NULL,
#endif
	STANDARD_MODULE_PROPERTIES_EX
};

/* implement standard "stub" routine to introduce ourselves to Zend */
#ifdef COMPILE_DL_TF_QUEUE
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(tf_queue)
#endif
