PHP_ARG_ENABLE(tf_queue, whether to enable tf_queue, [ --enable-tf_queue   Enable Tf_queue])

if test "$PHP_TF_QUEUE" = "yes"; then

	

	if ! test "x" = "x"; then
		PHP_EVAL_LIBLINE(, TF_QUEUE_SHARED_LIBADD)
	fi

	AC_DEFINE(HAVE_TF_QUEUE, 1, [Whether you have Tf_queue])
	tf_queue_sources="tf_queue.c kernel/main.c kernel/memory.c kernel/exception.c kernel/debug.c kernel/backtrace.c kernel/object.c kernel/array.c kernel/string.c kernel/fcall.c kernel/require.c kernel/file.c kernel/operators.c kernel/math.c kernel/concat.c kernel/variables.c kernel/filter.c kernel/iterator.c kernel/time.c kernel/exit.c twistersfury/phalcon/queue/job/jobinterface.zep.c
	twistersfury/phalcon/queue/job/abstractjob.zep.c
	twistersfury/phalcon/queue/cli/task/runjobtask.zep.c
	twistersfury/phalcon/queue/exceptions/nonfatalexception.zep.c
	twistersfury/phalcon/queue/adapter/beanstalk.zep.c
	twistersfury/phalcon/queue/adapter/beanstalk/exception.zep.c
	twistersfury/phalcon/queue/adapter/beanstalk/job.zep.c
	twistersfury/phalcon/queue/cli/task/queuetask.zep.c
	twistersfury/phalcon/queue/cli/task/runjob/exittask.zep.c
	twistersfury/phalcon/queue/di/serviceprovider/queue.zep.c
	twistersfury/phalcon/queue/exceptions/exitexception.zep.c
	twistersfury/phalcon/queue/exceptions/fatalexception.zep.c
	twistersfury/phalcon/queue/exceptions/nonfatalexception/release.zep.c
	twistersfury/phalcon/queue/exceptions/restart.zep.c
	twistersfury/phalcon/queue/job/exitjob.zep.c
	twistersfury/phalcon/queue/job/keepalive.zep.c
	twistersfury/phalcon/queue/job/restart.zep.c
	twistersfury/phalcon/queue/manager.zep.c
	twistersfury/phalcon/queue/0__closure.zep.c
	twistersfury/phalcon/queue/1__closure.zep.c "
	PHP_NEW_EXTENSION(tf_queue, $tf_queue_sources, $ext_shared,, )
	PHP_ADD_BUILD_DIR([$ext_builddir/kernel/])
	for dir in "twistersfury/phalcon/queue twistersfury/phalcon/queue/adapter twistersfury/phalcon/queue/adapter/beanstalk twistersfury/phalcon/queue/cli/task twistersfury/phalcon/queue/cli/task/runjob twistersfury/phalcon/queue/di/serviceprovider twistersfury/phalcon/queue/exceptions twistersfury/phalcon/queue/exceptions/nonfatalexception twistersfury/phalcon/queue/job"; do
		PHP_ADD_BUILD_DIR([$ext_builddir/$dir])
	done
	PHP_SUBST(TF_QUEUE_SHARED_LIBADD)

	old_CPPFLAGS=$CPPFLAGS
	CPPFLAGS="$CPPFLAGS $INCLUDES"

	AC_CHECK_DECL(
		[HAVE_BUNDLED_PCRE],
		[
			AC_CHECK_HEADERS(
				[ext/pcre/php_pcre.h],
				[
					PHP_ADD_EXTENSION_DEP([tf_queue], [pcre])
					AC_DEFINE([ZEPHIR_USE_PHP_PCRE], [1], [Whether PHP pcre extension is present at compile time])
				],
				,
				[[#include "main/php.h"]]
			)
		],
		,
		[[#include "php_config.h"]]
	)

	AC_CHECK_DECL(
		[HAVE_JSON],
		[
			AC_CHECK_HEADERS(
				[ext/json/php_json.h],
				[
					PHP_ADD_EXTENSION_DEP([tf_queue], [json])
					AC_DEFINE([ZEPHIR_USE_PHP_JSON], [1], [Whether PHP json extension is present at compile time])
				],
				,
				[[#include "main/php.h"]]
			)
		],
		,
		[[#include "php_config.h"]]
	)

	CPPFLAGS=$old_CPPFLAGS

	PHP_INSTALL_HEADERS([ext/tf_queue], [php_TF_QUEUE.h])

fi
