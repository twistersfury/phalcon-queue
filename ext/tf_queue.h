
/* This file was generated automatically by Zephir do not modify it! */

#ifndef ZEPHIR_CLASS_ENTRIES_H
#define ZEPHIR_CLASS_ENTRIES_H

#include "twistersfury/phalcon/queue/job/jobinterface.zep.h"
#include "twistersfury/phalcon/queue/job/abstractjob.zep.h"
#include "twistersfury/phalcon/queue/cli/task/runjobtask.zep.h"
#include "twistersfury/phalcon/queue/exceptions/nonfatalexception.zep.h"
#include "twistersfury/phalcon/queue/adapter/beanstalk.zep.h"
#include "twistersfury/phalcon/queue/adapter/beanstalk/exception.zep.h"
#include "twistersfury/phalcon/queue/adapter/beanstalk/job.zep.h"
#include "twistersfury/phalcon/queue/cli/task/queuetask.zep.h"
#include "twistersfury/phalcon/queue/cli/task/runjob/exittask.zep.h"
#include "twistersfury/phalcon/queue/di/serviceprovider/queue.zep.h"
#include "twistersfury/phalcon/queue/exceptions/exitexception.zep.h"
#include "twistersfury/phalcon/queue/exceptions/fatalexception.zep.h"
#include "twistersfury/phalcon/queue/exceptions/nonfatalexception/release.zep.h"
#include "twistersfury/phalcon/queue/exceptions/restart.zep.h"
#include "twistersfury/phalcon/queue/job/exitjob.zep.h"
#include "twistersfury/phalcon/queue/job/keepalive.zep.h"
#include "twistersfury/phalcon/queue/job/restart.zep.h"
#include "twistersfury/phalcon/queue/manager.zep.h"
#include "twistersfury/phalcon/queue/0__closure.zep.h"
#include "twistersfury/phalcon/queue/1__closure.zep.h"

#endif