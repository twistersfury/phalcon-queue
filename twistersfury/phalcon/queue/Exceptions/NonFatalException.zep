namespace TwistersFury\Phalcon\Queue\Exceptions;

use RuntimeException;

class NonFatalException extends RuntimeException
{

}
