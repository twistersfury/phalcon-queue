namespace TwistersFury\Phalcon\Queue\Exceptions\NonFatalException;

use TwistersFury\Phalcon\Queue\Exceptions\NonFatalException;

class Release extends NonFatalException
{
    protected message = "Job Scheduled For Release";

    /**
     * @var int
     */
    private delay = 0 {
        get, set
    };
}
