namespace TwistersFury\Phalcon\Queue;

use Phalcon\Di\Injectable;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class Manager extends Injectable
{
    public function getDriver(string! queueName) -> <Beanstalk>
    {
        var queue = this->getDi()->get("queue");

        queue->choose(queueName);
        queue->watch(queueName);

        if queueName !== "default" {
            queue->ignore("default");
        }

        return queue;
    }

    public function put(string! queueName, <JobInterface> job, array options = null) -> <Manager>
    {
        this->getDriver(queueName)->put(
            job,
            options
        );

        return this;
    }

    public function reserve(string! queueName, int! timeout = 0) -> bool|<JobInterface>
    {
        return this->getDriver(queueName)->reserve(timeout);
    }
}
