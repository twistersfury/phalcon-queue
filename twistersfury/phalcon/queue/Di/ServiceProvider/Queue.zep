namespace TwistersFury\Phalcon\Queue\Di\ServiceProvider;

use Phalcon\Config\Config;
use TwistersFury\Shared\Di\ServiceProvider\AbstractServiceProvider;

class Queue extends AbstractServiceProvider
{
    protected function registerQueues() -> void
    {
        if !this->{"get"}("config")->has("queues") {
            return;
        //JIC
        } elseif !this->{"has"}("baseQueue") {
            this->registerBaseQueue();
        }

        var instance, config, name;

        for name, config in this->{"get"}("config")->queues->getIterator() {
            let instance = this->{"get"}("baseQueue", [config]);
            this->{"setShared"}(
                name,
                instance
            );
        }
    }

    protected function registerManager() -> void
    {
        this->{"setShared"}(
            "queueManager",
            "TwistersFury\\Phalcon\\Queue\\Manager"
        );
    }

    protected function registerQueue() -> void
    {
        this->{"setShared"}(
            "queue",
            function (<Config> config = null) {
                return this->{"get"}(
                    "baseQueue",
                    config
                );
            }
        );
    }

    protected function registerBaseQueue() -> void
    {
        this->{"set"}(
            "baseQueue",
            function (<Config> config = null) {
                if config === null {
                    let config = this->{"getShared"}("config")->queue;
                }

                var queue;

                let queue = this->{"get"}(
                    "TwistersFury\\Phalcon\\Queue\\Adapter\\Beanstalk",
                    [[
                        "host": config->host,
                        "port": config->port
                    ]]
                );

                if config->has("tube") {
                    queue->choose(config->tube);
                    queue->watch(config->tube);
                    queue->ignore("default");

                    if config->has("keepAlive") {
                        queue->put(
                            this->{"get"}("TwistersFury\\Phalcon\\Queue\\Job\\KeepAlive")
                        );
                    }
                }

                return queue;
            }
        );
    }
}
