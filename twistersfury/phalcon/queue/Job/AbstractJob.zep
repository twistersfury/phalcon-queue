namespace TwistersFury\Phalcon\Queue\Job;

use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Di\Injectable;
use Phalcon\Di\InjectionAwareInterface;
use Serializable;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\Shared\Di\AbstractConfigInjectable;

abstract class AbstractJob extends AbstractConfigInjectable implements JobInterface, InjectionAwareInterface, Serializable
{
    public function getOptions() -> array
    {
        return [];
    }
}
