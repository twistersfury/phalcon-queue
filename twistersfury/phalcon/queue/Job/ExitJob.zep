namespace TwistersFury\Phalcon\Queue\Job;

use Phalcon\Di\Injectable;
use Phalcon\Di\InjectionAwareInterface;
use TwistersFury\Phalcon\Queue\Exceptions\ExitException;

class ExitJob extends AbstractJob
{
    public function handle() -> <JobInterface>
    {
        throw new ExitException("Exit Job");
    }
}
