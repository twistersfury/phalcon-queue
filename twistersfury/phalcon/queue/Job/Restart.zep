namespace TwistersFury\Phalcon\Queue\Job;

use Phalcon\Di\Injectable;
use Phalcon\Di\InjectionAwareInterface;
use TwistersFury\Phalcon\Queue\Exceptions\Restart as RestartException;

class Restart extends AbstractJob
{
    public function handle() -> <JobInterface>
    {
        throw new RestartException("Restart Job");
    }
}
