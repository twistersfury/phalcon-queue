namespace TwistersFury\Phalcon\Queue\Job;

interface JobInterface
{
    public function handle() -> <JobInterface>;
    public function getOptions() -> array;
}
