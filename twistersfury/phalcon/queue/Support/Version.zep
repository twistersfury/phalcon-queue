namespace TwistersFury\Phalcon\Queue\Support;

class Version
{
    public function get()
    {
        return phpversion("tf_queue");
    }
}
