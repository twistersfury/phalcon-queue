namespace TwistersFury\Phalcon\Queue\Cli\Task;

use Phalcon\Cli\Task;
use InvalidArgumentException;

class RunJobTask extends Task
{
    public function mainAction(string! className = "", string! serviceName = "queue") -> <QueueTask>
    {
        if className === "" && method_exists(this, "getJobClass") {
            var jobClass = this->{"getJobClass"}();
            let className = jobClass;
        }

        if !className {
            throw new InvalidArgumentException("Invalid Class Name");
        }

        this->{"logger"}->info(
            "Running Job",
            [
                "job": className
            ]
        );

        this->getDi()->get(serviceName)->put(
            this->getDi()->get(
                className
            ),
            this->getOptions()
        );

        return this;
    }

    protected function getOptions() -> array
    {
        return [];
    }
}
