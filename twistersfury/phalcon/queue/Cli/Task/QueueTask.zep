namespace TwistersFury\Phalcon\Queue\Cli\Task;

use DateTime;
use Phalcon\Cli\Task;
use Phalcon\Storage\Exception;
use Throwable;
use TwistersFury\Phalcon\Queue\Exceptions\ExitException;
use TwistersFury\Phalcon\Queue\Exceptions\Restart;
use TwistersFury\Phalcon\Queue\Exceptions\FatalException;
use TwistersFury\Phalcon\Queue\Exceptions\NonFatalException;
use TwistersFury\Phalcon\Queue\Exceptions\NonFatalException\Release;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\Phalcon\Queue\Job\KeepAlive;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk\Job;

class QueueTask extends Task
{
    const RESTART_KEY = "TwistersFury.Phalcon.Queue.Task.Restart";
    private startTime {
        get
    };

    public function consumeAction(string! serviceName = "queue", int maxItems = 0, int reserveTimeout = 0, int sleepMs = 0) -> <QueueTask>
    {
        int count = 0;

        this->{"logger"}->info(
            "Queue Started",
            [
                "service": serviceName,
                "maxItems" : maxItems
            ]
        );

        var job, exception;

        bool requeue = false;

        let this->startTime = this->getNow();

        while this->canProcess() && (maxItems === 0 || count < maxItems) {
            let job = this->getDi()->get(serviceName)->reserve(reserveTimeout);
            if job === false {
                //If sleepMs Is 0, Exit Immediately. Other Sleep X Milliseconds.
                if sleepMs === 0 {
                    break;
                }

                usleep(sleepMs);

                continue;
            } elseif job instanceof KeepAlive {
                job->delete();
                let requeue = true;

                continue;
            }

            let count = count + 1;

            this->{"logger"}->debug(
                "Queue Loop Count",
                [
                    "count": count
                ]
            );

            //TODO: Convert To Yield

            try {
                this->processJob(job);
            } catch ExitException, exception {
                this->{"logger"}->notice(
                    "Exit Initialized",
                    [
                        "message": exception->getMessage()
                    ]
                );

                job->delete();

                throw exception;
            }

            if sleepMs > 0 {
                usleep(sleepMs);
            }
        }

        if requeue === true {
            this->getDi()->get(serviceName)->put(
                this->getDi()->get("TwistersFury\\Phalcon\\Queue\\Job\\KeepAlive")
            );
        }

        this->{"logger"}->info("Queue Ended");

        return this;
    }

    protected function processJob(<Job> job) -> <QueueTask>
    {
        var exception;

        try {
            if unlikely !is_object(job->getBody()) || !(job->getBody() instanceof JobInterface) {
                this->{"logger"}->debug(
                    "Job Does Not Implement JobInterface",
                    [
                        "class": is_object(job->getBody()) ? get_class(job->getBody()) : job->getBody()
                    ]
                );

                throw new FatalException("Job Does Not Implement JobInterface");
            }

            this->{"logger"}->info(
                "Processing Queue Job",
                [
                    "class": get_class(job->getBody()),
                    "stats": job->stats()
                ]
            );

            job->getBody()->handle();
            job->delete();
        } catch Restart, exception {
            this->{"logger"}->notice(
                "Restart Initialized",
                [
                    "message": exception->getMessage()
                ]
            );

            this->registerRestart();

            job->delete();
        } catch ExitException, exception {
            throw exception;
        } catch FatalException, exception {
            this->{"logger"}->error(
                "Fatal Exception",
                [
                    "message": exception->getMessage(),
                    "line": exception->getLine(),
                    "file": exception->getFile()
                ]
            );

            job->delete();
        } catch Release, exception {
            this->{"logger"}->notice(
                "Releasing Job",
                [
                    "message": exception->getMessage(),
                    "delay": exception->getDelay()
                ]
            );

            job->release(100, exception->getDelay());
        } catch NonFatalException, exception {
            this->{"logger"}->notice(
                "Non Fatal Exception",
                [
                    "message": exception->getMessage()
                ]
            );

            job->release();
        } catch Throwable, exception {
            this->{"logger"}->error(exception->getMessage());

            job->delete();
        }

        this->{"logger"}->info("Done Processing Queue Job");

        return this;
    }

    protected function canProcess() -> bool
    {
        if unlikely !this->getDi()->has("cache") {
            return true;
        } elseif !this->{"cache"}->has(this->getRestartKey()) {
            return true;
        }

        try {
            return this->buildDate(
                this->{"cache"}->get(this->getRestartKey())
            ) < this->getStartTime();
        }

        return false;
    }

    protected function buildDate(string! dateTime) -> <DateTime>
    {
        return this->getDi()->get("DateTime", [dateTime]);
    }

    protected function getRestartKey() -> string
    {
        return QueueTask::RESTART_KEY;
    }

    protected function getNow() -> <DateTime>
    {
        return this->getDi()->get("DateTime");
    }

    protected function registerRestart() -> <QueueTask>
    {
        if likely this->getDi()->has("cache") {
            var now;

            let now = this->getNow();

            this->{"logger"}->debug(
                "Restart Cache",
                [
                    "key": this->getRestartKey(),
                    "date": now->format("Y-m-d H:i:s")
                ]
            );

            var result;

            let result = this->{"cache"}->set(
                this->getRestartKey(),
                now->format("Y-m-d H:i:s")
            );

            if unlikely result === false {
                throw new Exception("Unable To Save Restart");
            }
        }

        return this;
    }
}
