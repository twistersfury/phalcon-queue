namespace TwistersFury\Phalcon\Queue\Cli\Task\RunJob;

use TwistersFury\Phalcon\Queue\Cli\Task\RunJobTask;

class ExitTask extends RunJobTask
{
    protected function getJobClass() -> string
    {
        return "TwistersFury\\Phalcon\\Queue\\Job\\ExitJob";
    }

    protected function getOptions() -> array
    {
        return array_merge(
            parent::getOptions(),
            [
                "priority": 0 //Give It The Highest Priority
            ]
        );
    }
}
