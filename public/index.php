<?php

use TwistersFury\Phalcon\Queue\Di\Http;
use TwistersFury\Phalcon\Queue\Kernel;

try {
    $bootstrap   = new Kernel(new Http(), dirname(__DIR__));
    $application = $bootstrap->getApplication();

    echo $application->handle($_GET['_url'] ?? "/")->getContent();
} catch (Exception $exception) {
    if (ini_get('tf_queue.debug.mode')) {
        (new \Phalcon\Debug())->onUncaughtException($exception);
        exit;
    }

    echo $application->handle("/support/system/error")->getContent();
} catch (Error $exception) {
    if (ini_get('tf_queue.debug.mode')) {
        throw $exception;
    }

    echo $application->handle("/support/system/error")->getContent();
}
